<?php

namespace App\Http\Controllers;

use App\ItemOption;
use App\Option;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;


class ApiController extends Controller
{


    /**
     * API Data JSON
     *
     * @return JSON
     */
    public function varData($id)
    {

        $itemOptions = ItemOption::where('item', $id)->get();
        $collection = new Collection();
        foreach ($itemOptions as $item) {
            $collection->push(Option::where('id', $item->option)->first());
        }

        $collection = $collection->toJson(JSON_PRETTY_PRINT);
        return $collection;
    }


}
