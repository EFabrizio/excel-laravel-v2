<?php

namespace App\Http\Controllers;

use App\Item;
use App\ItemOption;
use App\Option;
use App\Setting;
use Illuminate\Http\Request;

class SettingsController extends Controller
{

    /**
     * Show the website settings view.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showWebSettings()
    {
        $settings = Setting::all();
        return view('dashboard/settings/website', compact('settings'));
    }

    /**
     * Show the var settings view.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showVarSettings() {
        $items = Item::all();
        foreach ($items as $item) {
            $item->options = ItemOption::where('item', $item->id)->count();
        }

        return view('dashboard/settings/var', compact('items'));
    }

    /**
     * Show the var settings view.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showItemOptions($id) {
        $itemOptions = ItemOption::where('item', $id)->get();
        foreach ($itemOptions as $item) {
            $item->option = Option::where('id', $item->option)->first();

        }
        return view('dashboard/settings/itemOptions', compact('itemOptions'));
    }

    /**
     * Show the var edit settings view.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showEditOption($id) {
        $option = Option::where('id', $id)->first();
        return view('dashboard/settings/editOption', compact('option'));
    }


    /**
     * Show the var settings view.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showRemoveOption($id) {
        $option = Option::where('id', $id)->first();
        return view('dashboard/settings/deleteOption', compact('option'));
    }


    /**
     * @param Request $request
     * @return mixed
     */
    protected function editOption(Request $request)
    {

        $option = Option::where('id', $request['option_id'])->first();
        if ($option) {
            $option->name = $request['name'];
            $option->name = $request['name'];
            $option->name = $request['name'];
            $option->save();
            return redirect()->route('varSettings')->with('message', 'Se editó correctamente.');
        } else {
            return redirect()->route('varSettings')->with('error', 'Esta VAR no existe');
        }

    }

    /**
     * @param Request $request
     * @return mixed
     */
    protected function removeOption(Request $request)
    {

        $option = Option::where('id', $request['option_id'])->first();

        if ($option) {
            $itemOptions = ItemOption::where('option', $option->id)->get();
            foreach ($itemOptions as $item){
                $item->delete();
            }
            $option->delete();
            return redirect()->route('varSettings')->with('message', 'Se eliminó correctamente.');
        } else {
            return redirect()->route('varSettings')->with('error', 'Esta VAR no existe');
        }

    }
}
