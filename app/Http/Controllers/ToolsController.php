<?php

namespace App\Http\Controllers;

use App\ItemOption;
use App\Option;
use Illuminate\Http\Request;

class ToolsController extends Controller
{


    /**
     * Show toldas view.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showToldas()
    {

        $itemOptions = ItemOption::where('item', 1)->limit(2)->get();
        $tableOptions = ItemOption::where('item', 3)->get();

        foreach ($tableOptions as $item) {
            $item->option = Option::where('id', $item->option)->first();

        }

        foreach ($itemOptions as $item) {
            $item->option = Option::where('id', $item->option)->first();

        }

        return view('dashboard/tools/toldas', compact('itemOptions', 'tableOptions'));
    }

    /**
     * Show carpas view.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showCarpas()
    {

        $itemOptions = ItemOption::where('item', 2)->limit(2)->get();
        foreach ($itemOptions as $item) {
            $item->option = Option::where('id', $item->option)->first();

        }

        return view('dashboard/tools/carpas', compact('itemOptions'));
    }

    /**
     * Show cortes view.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showCorte()
    {

        $itemOptions = ItemOption::where('item', 6)->get();
        $tableOptions = ItemOption::where('item', 47)->get();
        foreach ($itemOptions as $item) {
            $item->option = Option::where('id', $item->option)->first();

        }

        foreach ($tableOptions as $item) {
            $item->option = Option::where('id', $item->option)->first();

        }

        return view('dashboard/tools/corte', compact('itemOptions', 'tableOptions'));
    }

    /**
     * Show toldas view.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showLuminosos()
    {

        $itemOptions = ItemOption::where('item', 7)->get();
        $electric = ItemOption::where('item', 8)->get();
        $faces = ItemOption::where('item', 9)->get();
        $tableOptions = ItemOption::where('item', 3)->get();

        foreach ($faces as $item) {
            $item->option = Option::where('id', $item->option)->first();

        }

        foreach ($electric as $item) {
            $item->option = Option::where('id', $item->option)->first();

        }

        foreach ($tableOptions as $item) {
            $item->option = Option::where('id', $item->option)->first();

        }

        foreach ($itemOptions as $item) {
            $item->option = Option::where('id', $item->option)->first();

        }

        return view('dashboard/tools/luminosos', compact('itemOptions', 'tableOptions', 'electric', 'faces'));
    }

    /**
     * Show toldas view.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showFacias()
    {

        $itemOptions = ItemOption::where('item', 38)->get();
        $tableOptions = ItemOption::where('item', 3)->get();

        foreach ($tableOptions as $item) {
            $item->option = Option::where('id', $item->option)->first();

        }

        foreach ($itemOptions as $item) {
            $item->option = Option::where('id', $item->option)->first();

        }

        return view('dashboard/tools/facias', compact('itemOptions', 'tableOptions'));
    }

    /**
     * Show carpas view.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showImpresion()
    {

        $itemOptions = ItemOption::where('item', 31)->get();

        $equips = ItemOption::where('item', 32)->get();

        $metalics = ItemOption::where('item', 37)->get();


        foreach ($metalics as $item) {
            $item->option = Option::where('id', $item->option)->first();
        }

        foreach ($equips as $item) {
            $item->option = Option::where('id', $item->option)->first();
        }

        foreach ($itemOptions as $item) {
            $item->option = Option::where('id', $item->option)->first();
        }

        return view('dashboard/tools/impresion', compact('itemOptions', 'equips', 'metalics'));
    }

    public function showImpresionStandard()
    {

        $itemOptions = ItemOption::where('item', 49)->get();

        foreach ($itemOptions as $item) {
            $item->option = Option::where('id', $item->option)->first();
        }

        return view('dashboard/tools/impresionStandard', compact('itemOptions'));
    }


    /**
     * Show carpas view.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showPapeleria()
    {

        $itemOptions = ItemOption::where('item', 14)->get();
        $faces = ItemOption::where('item', 15)->get();
        $laminado = ItemOption::where('item', 16)->get();

        $itemsTwo = ItemOption::where('item', 17)->get();

        $troquelado = ItemOption::where('item', 18)->get();

        foreach ($troquelado as $item) {
            $item->option = Option::where('id', $item->option)->first();

        }

        foreach ($laminado as $item) {
            $item->option = Option::where('id', $item->option)->first();

        }

        foreach ($faces as $item) {
            $item->option = Option::where('id', $item->option)->first();

        }

        foreach ($itemsTwo as $item) {
            $item->option = Option::where('id', $item->option)->first();

        }

        foreach ($itemOptions as $item) {
            $item->option = Option::where('id', $item->option)->first();

        }

        return view('dashboard/tools/papeleria', compact('itemOptions', 'itemsTwo', 'troquelado', 'faces', 'laminado'));
    }

    public function showSuvenir()
    {

        $itemOptions = ItemOption::where('item', 50)->get();

        foreach ($itemOptions as $item) {
            $item->option = Option::where('id', $item->option)->first();
        }

        return view('dashboard/tools/suvenir', compact('itemOptions'));
    }

    public function showOtros()
    {

        $itemOptions = ItemOption::where('item', 53)->get();

        foreach ($itemOptions as $item) {
            $item->option = Option::where('id', $item->option)->first();
        }

        return view('dashboard/tools/otros', compact('itemOptions'));
    }



}
