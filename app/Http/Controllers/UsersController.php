<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{

    /**
     * Show users page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function usersPage()
    {
        $users = User::all();

        return view('dashboard/users/index', compact('users'));
    }

    /**
     * Show users add page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function addUserPage()
    {

        return view('dashboard/users/add');
    }


    protected function createUser(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'rank' => 'required',
            'password' => 'confirmed|min:6',
        ]);

        $user = User::create([
            'rank' => $request['rank'],
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);


        return redirect()->route('users')->with('message', 'Se agregó correctamente el usuario.');

    }

    /**
     * Show users edit page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function usersEdit($id)
    {

        $user = User::where('id', $id)->first();

        if ($user) {
            return view('dashboard/users/edit', compact('user'));
        } else {
            return redirect()->route('users')->with('error', 'El usuario no existe');
        }
    }

    /**
     * Show users delete page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function usersDelete($id)
    {

        $user = User::where('id', $id)->first();

        if ($user) {
            return view('dashboard/users/delete', compact('user'));
        } else {
            return redirect()->route('users')->with('error', 'El usuario no existe');
        }
    }

    /**
     * Remove an user from the database.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function removeUser(Request $request)
    {

        $user = User::where('id', $request['user_id'])->first();
        if ($user) {
            $user->delete();
            return redirect()->route('users')->with('message', 'Se eliminó correctamente el usuario.');
        } else {
            return redirect()->route('users')->with('error', 'El usuario no existe');
        }

    }

    /**
     * A method to updates users data.
     * @param  array $data
     * @return \App\User
     */
    protected function updateUser(Request $request)
    {

        $this->validate($request, [
            'user_id' => 'required',
            'name' => 'required',
            'rank' => 'required',
        ]);

        $user = User::where('id', $request['user_id'])->first();


        if ($user) {

            $user->rank = $request['rank'];
            if ($request['password'] != '') {
                $this->validate($request, [
                    'password' => 'confirmed|min:6',
                ]);
                $user->password = Hash::make($request['password']);
            }

            $user->name = $request['name'];
            $user->rank = $request['rank'];
            $user->save();

            return redirect()->route('userEdit', ['id' => $request['user_id']])->with('message', 'Se editó correctamente el usuario.');
        } else {
            return redirect()->route('userEdit')->with('error', 'El usuario no existe');
        }

    }


}
