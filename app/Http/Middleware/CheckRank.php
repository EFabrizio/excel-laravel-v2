<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckRank
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param array $ranks
     * @return mixed
     */
    public function handle($request, Closure $next, ...$ranks)
    {

        $rankFound = false;

        foreach ($ranks as $rank) {
            if (Auth::user()->rank == $rank) {
                $rankFound = true;
            }
        }

        if ($rankFound) {
            return $next($request);
        } else {
            return redirect()->route('home');
        }

    }
}
