<?php

use Illuminate\Database\Seeder;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        DB::table('items')->insert([
            [
                'description' => 'Material Toldas'
            ],
            [
                'description' => 'Material Carpas'
            ],
            [
                'description' => 'Nombres de Sector'
            ],

            [
                'description' => 'Sector Toldas < 15 Pies'
            ],

            [
                'description' => 'Sector Toldas > 15 Pies'
            ],

            [
                'description' => 'Material Corte'
            ],

            [
                'description' => 'Material Iluminación'
            ],

            [
                'description' => 'Sistema Eléctrico Iluminación'
            ],

            [
                'description' => 'Caras Y Poste Iluminación'
            ],

            [
                'description' => 'Precio Pie Cuadrado'
            ],

            [
                'description' => 'Sector Con Poste Iluminación'
            ],

            [
                'description' => 'Sector Sin Poste, Letrero  <= 15 Pies Iluminación'
            ],

            [
                'description' => 'Sector Sin Poste, Letrero > 15 Pies Iluminación'
            ],

            [
                'description' => 'Material Uno Papeleria'
            ],

            [
                'description' => 'Caras Papeleria'
            ],

            [
                'description' => 'Laminado Papeleria'
            ],

            [
                'description' => 'Material Dos Papeleria'
            ],

            [
                'description' => 'Troquelado Papeleria'
            ],

            [
                'description' => 'Una cara 1 a 50 Papeleria'
            ],

            [
                'description' => 'Una cara 51 a 250 Papeleria'
            ],

            [
                'description' => 'Una cara 251 a 500 Papeleria'
            ],

            [
                'description' => 'Una cara 501 en adelante Papeleria'
            ],

            [
                'description' => 'Dos caras 1 a 50 Papeleria'
            ],

            [
                'description' => 'Dos caras 51 a 250 Papeleria'
            ],

            [
                'description' => 'Dos caras 251 a 500 Papeleria'
            ],

            [
                'description' => 'Dos caras 501 en adelante Papeleria'
            ],

            [
                'description' => 'Material Laminado de Papeleria'
            ],

            [
                'description' => 'Adhesivos Material Precio'
            ],

            [
                'description' => 'Troquelado 1-50 Material'
            ],

            [
                'description' => 'Troquelado 50-500 Material'
            ],

            [
                'description' => 'Material Impresion'
            ],

            [
                'description' => 'Equipo Impresion'
            ],

            [
                'description' => 'Material Times Impresion'
            ],

            [
                'description' => 'Material Mimaki Impresion'
            ],

            [
                'description' => 'Material DGI Impresion'
            ],

            [
                'description' => 'Material Ricoh Impresion'
            ],

            [
                'description' => 'Laminado Impresion'
            ],

            [
                'description' => 'Material Facias'
            ],

            [
                'description' => 'Sector menos de 15 pies Facias'
            ],


            [
                'description' => 'Sector entre 15 y 40 pies Facias'
            ],


            [
                'description' => 'Sector más de 40 pies Facias'
            ],


            [
                'description' => 'Precio mas 20 pies en aluminio Facias'
            ],

            [
                'description' => 'Precio mas de 20 pies en hierro'
            ],

            [
                'description' => 'Precio menos de 20 en aluminio Facias'
            ],

            [
                'description' => 'Precio menos de 20 pies en hierro Facias'
            ],

            [
                'description' => 'Laminado Precios Impresion'
            ],

            [
                'description' => 'Sector Corte'
            ],

            [
                'description' => 'Sector Corte Price'
            ],

            [
                'description' => 'Impresion Estandarizada'
            ],

            [
                'description' => 'Suvenir Material'
            ],

            [
                'description' => 'Suvenir Precio Unidades'
            ],

            [
                'description' => 'Suvenir Precio +12 Unidades'
            ],

            [
                'description' => 'Otros Material'
            ],

            [
                'description' => 'Otros Precio'
            ],

            [
                'description' => 'Otros Precio +12 Unidades'
            ],


        ]);


        /*
         * Carpas
         */
        DB::table('options')->insert([
            [
                'name' => 'Sumbrella',
                'value' => '8',
                'description' => 'Carpas'
            ],
            [
                'name' => 'Lona plástica',
                'value' => '6',
                'description' => 'Carpas'
            ],

            [
                'name' => '10-15',
                'value' => '9.5',
                'description' => 'Carpas'
            ],

            [
                'name' => 'Sumbrella',
                'value' => '11',
                'description' => 'Toldas'
            ],
            [
                'name' => 'Lona plástica',
                'value' => '9.50',
                'description' => 'Toldas'
            ],


            [
                'name' => '0-10',
                'value' => '13',
                'description' => 'Toldas'
            ],


            [
                'name' => '10-999',
                'value' => '8.50',
                'description' => 'Toldas'
            ],
        ]);


        /*
         * Toldas Sector
         */
        DB::table('options')->insert([
            [
                'name' => 'Chitré',
                'value' => 'Chitré',
                'description' => 'Localización'
            ],

            [
                'name' => 'Penonomé',
                'value' => 'Penonomé',
                'description' => 'Localización'
            ],

            [
                'name' => 'Coronado',
                'value' => 'Coronado',
                'description' => 'Localización'
            ],
            [
                'name' => 'Panamá',
                'value' => 'Panamá',
                'description' => 'Localización'
            ],

            [
                'name' => 'Bocas del Toro',
                'value' => 'Bocas del Toro',
                'description' => 'Localización'
            ],

            [
                'name' => 'Pesé',
                'value' => 'Pesé',
                'description' => 'Localización'
            ],

            [
                'name' => 'Las Minas',
                'value' => 'Las Minas',
                'description' => 'Localización'
            ],

            [
                'name' => 'Tonosí',
                'value' => 'Tonosí',
                'description' => 'Localización'
            ],

            [
                'name' => 'Chorrera',
                'value' => 'Chorrera',
                'description' => 'Localización'
            ],

            [
                'name' => 'Darién',
                'value' => 'Darién',
                'description' => 'Localización'
            ],

            [
                'name' => 'Parita',
                'value' => 'Parita',
                'description' => 'Localización'
            ],

            [
                'name' => 'Los Pozos',
                'value' => 'Los Pozos',
                'description' => 'Localización'
            ],

            [
                'name' => 'Rio Hato',
                'value' => 'Rio Hato',
                'description' => 'Localización'
            ],

            [
                'name' => 'Chiriquí',
                'value' => 'Chiriquí',
                'description' => 'Localización'
            ],

            [
                'name' => '',
                'value' => '',
                'description' => 'Localización vacía'
            ],

            [
                'name' => 'Los Santos',
                'value' => 'Los Santos',
                'description' => 'Localización'
            ],

            [
                'name' => 'Santiago',
                'value' => 'Santiago',
                'description' => 'Localización'
            ],

            [
                'name' => 'Antón',
                'value' => 'Antón',
                'description' => 'Localización'
            ],

            [
                'name' => 'Colón',
                'value' => 'Colón',
                'description' => 'Localización'
            ],

            [
                'name' => '',
                'value' => '',
                'description' => 'Localización vacía'
            ],

            [
                'name' => 'Monagrillo',
                'value' => 'Monagrillo',
                'description' => 'Localización'
            ],

            [
                'name' => 'Pedasí',
                'value' => 'Pedasí',
                'description' => 'Localización'
            ],

            [
                'name' => '',
                'value' => '',
                'description' => 'Localización vacía'
            ],

            [
                'name' => '',
                'value' => '',
                'description' => 'Localización vacía'
            ],

            [
                'name' => '',
                'value' => '',
                'description' => 'Localización vacía'
            ],

            [
                'name' => 'La Arena',
                'value' => 'La Arena',
                'description' => 'Localización'
            ],

            [
                'name' => 'El Roble',
                'value' => 'El Roble',
                'description' => 'Localización'
            ],

            [
                'name' => '',
                'value' => '',
                'description' => 'Localización vacía'
            ],

            [
                'name' => '',
                'value' => '',
                'description' => 'Localización vacía'
            ],

            [
                'name' => '',
                'value' => '',
                'description' => 'Localización vacía'
            ],

            [
                'name' => 'Las Tablas',
                'value' => 'Las Tablas',
                'description' => 'Localización'
            ],

            [
                'name' => 'Macaracas',
                'value' => 'Macaracas',
                'description' => 'Localización'
            ],

            [
                'name' => '',
                'value' => '',
                'description' => 'Localización vacía'
            ],

            [
                'name' => '',
                'value' => '',
                'description' => 'Localización vacía'
            ],

            [
                'name' => '',
                'value' => '',
                'description' => 'Localización vacía'
            ],

            [
                'name' => 'Guararé',
                'value' => 'Guararé',
                'description' => 'Localización'
            ],

            [
                'name' => 'Aguadulce',
                'value' => 'Aguadulce',
                'description' => 'Localización'
            ],

            [
                'name' => '',
                'value' => '',
                'description' => 'Localización vacía'
            ],

            [
                'name' => '',
                'value' => '',
                'description' => 'Localización vacía'
            ],

            [
                'name' => '',
                'value' => '',
                'description' => 'Localización vacía'
            ],


        ]);

        /*
         * Toldas Sector < 15 Pies
         */
        DB::table('options')->insert([
            [
                'name' => 'A',
                'value' => '45',
                'description' => 'Sector < 15 Pies'
            ],

            [
                'name' => 'B',
                'value' => '120',
                'description' => 'Sector < 15 Pies'
            ],

            [
                'name' => 'C',
                'value' => '160',
                'description' => 'Sector < 15 Pies'
            ],

            [
                'name' => 'D',
                'value' => '280',
                'description' => 'Sector < 15 Pies'
            ],

            [
                'name' => 'E',
                'value' => '550',
                'description' => 'Sector < 15 Pies'
            ],
        ]);


        /*
         * Toldas Sector > 15 Pies
         */
        DB::table('options')->insert([
            [
                'name' => 'A',
                'value' => '65',
                'description' => 'Sector > 15 Pies'
            ],

            [
                'name' => 'B',
                'value' => '160',
                'description' => 'Sector > 15 Pies'
            ],

            [
                'name' => 'C',
                'value' => '200',
                'description' => 'Sector > 15 Pies'
            ],

            [
                'name' => 'D',
                'value' => '350',
                'description' => 'Sector > 15 Pies'
            ],

            [
                'name' => 'E',
                'value' => '635',
                'description' => 'Sector > 15 Pies'
            ],
        ]);


        /*
         * Material Corte
         */
        DB::table('options')->insert([
            [
                'name' => 'Vinil adhesivo LG con transfer de 4" a 18"',
                'value' => '2.50',
                'description' => 'Material Corte'
            ],

            [
                'name' => 'Vinil adhesivo LG con transfer de 24" a 30"',
                'value' => '3.00',
                'description' => 'Material Corte'
            ],

            [
                'name' => 'Vinil adhesivo LG con transfer de 48"',
                'value' => '3.50',
                'description' => 'Material Corte'
            ],

            [
                'name' => 'Vinil adhesivo con vinil de 2 a 3 capas con transfer de 4" a 18"',
                'value' => '4.00',
                'description' => 'Material Corte'
            ],

            [
                'name' => 'Esmerilado LG tono gris 5510 (el comun) con transfer de 4" a 18"',
                'value' => '3.00',
                'description' => 'Material Corte'
            ],


            [
                'name' => 'Esmerilado LG tono gris FROST (empañado) 5520 con transfer de 4" a 18"',
                'value' => '3.50',
                'description' => 'Material Corte'
            ],


            [
                'name' => 'Esmerilado LG tono blanco 5521 con transfer de 4" a 18"',
                'value' => '3.50',
                'description' => 'Material Corte'
            ],


            [
                'name' => 'Esmerilado LG tono escarchado 8082 con transfer de 4" a 18"',
                'value' => '5.00',
                'description' => 'Material Corte'
            ],


        ]);

        /*
        * Iluminación
        */
        DB::table('options')->insert([
            [
                'name' => 'Aluminio',
                'value' => 'Aluminio',
                'description' => 'Material Iluminación'
            ],

            [
                'name' => 'Hierro',
                'value' => 'Hierro',
                'description' => 'Material Iluminación'
            ],

            [
                'name' => 'Con luz',
                'value' => 'Con luz',
                'description' => 'Sistema eléctrico Iluminación'
            ],

            [
                'name' => 'Sin luz',
                'value' => 'Sin luz',
                'description' => 'Sistema eléctrico Iluminación'
            ],


            [
                'name' => '2 caras con poste',
                'value' => '2 caras con poste',
                'description' => 'Caras Iluminación'
            ],


            [
                'name' => '1 cara sin poste',
                'value' => '1 cara sin poste',
                'description' => 'Caras Iluminación'
            ],

            [
                'name' => 'Precio pie2',
                'value' => '40',
                'description' => 'Ancho <= 4, Alto <= 2, 2 caras con poste , con luz, Aluminio'
            ],

            [
                'name' => 'Precio pie2',
                'value' => '17',
                'description' => 'Ancho <= 4, Alto <= 5, 2 caras con poste , Sin luz, Hierro'
            ],

            [
                'name' => 'Precio pie2',
                'value' => '30',
                'description' => 'Ancho >= 4, Alto <= 5, 2 caras con poste , Con luz, Aluminio'
            ],

            [
                'name' => 'Precio pie2',
                'value' => '15',
                'description' => 'Ancho > 4, Alto <= 5, 2 caras con poste , Sin luz, Hierro'
            ],

            [
                'name' => 'Precio pie2',
                'value' => '7.50',
                'description' => 'Ancho > 4, Alto <= 5, 1 cara sin poste , Sin luz, Hierro'
            ],

            [
                'name' => 'Precio pie2',
                'value' => '20',
                'description' => 'Ancho <= 12, Alto <= 5, 1 cara sin poste , Con luz, Aluminio'
            ],


            [
                'name' => 'Precio pie2',
                'value' => '10',
                'description' => 'Ancho <= 10, Alto <= 5, 1 cara sin poste , Sin luz, Aluminio'
            ],


            [
                'name' => 'Precio pie2',
                'value' => '6.50',
                'description' => 'Ancho > 10, Alto <= 5, 1 cara sin poste , Sin luz, Hierro'
            ],

            [
                'name' => 'Null',
                'value' => '0.00',
                'description' => 'Null'
            ],

            [
                'name' => 'Precio pie2',
                'value' => '18.50',
                'description' => 'Ancho > 12, Alto <= 5, 1 cara sin poste , Con luz, Aluminio'
            ],

            [
                'name' => 'Precio pie2',
                'value' => '9',
                'description' => 'Ancho > 10, Alto <= 5, 1 cara sin poste , Sin Luz, Aluminio'
            ],

            [
                'name' => 'Instalación A',
                'value' => '95',
                'description' => 'Con poste'
            ],

            [
                'name' => 'Instalación B',
                'value' => '250',
                'description' => 'Con poste'
            ],

            [
                'name' => 'Instalación C',
                'value' => '380',
                'description' => 'Con poste'
            ],

            [
                'name' => 'Instalación D',
                'value' => '600',
                'description' => 'Con poste'
            ],

            [
                'name' => 'Instalación E',
                'value' => '800',
                'description' => 'Con poste'
            ],

            [
                'name' => 'Instalación A',
                'value' => '45',
                'description' => 'Sin poste hasta 15 pies de ancho'
            ],

            [
                'name' => 'Instalación B',
                'value' => '120',
                'description' => 'Sin poste hasta 15 pies de ancho'
            ],

            [
                'name' => 'Instalación C',
                'value' => '160',
                'description' => 'Sin poste hasta 15 pies de ancho'
            ],

            [
                'name' => 'Instalación D',
                'value' => '280',
                'description' => 'Sin poste hasta 15 pies de ancho'
            ],

            [
                'name' => 'Instalación E',
                'value' => '550',
                'description' => 'Sin poste hasta 15 pies de ancho'
            ],

            [
                'name' => 'Instalación A',
                'value' => '65',
                'description' => 'Sin poste letrero mayor a 15 pies de ancho'
            ],

            [
                'name' => 'Instalación B',
                'value' => '160',
                'description' => 'Sin poste letrero mayor a 15 pies de ancho'
            ],

            [
                'name' => 'Instalación C',
                'value' => '200',
                'description' => 'Sin poste letrero mayor a 15 pies de ancho'
            ],

            [
                'name' => 'Instalación D',
                'value' => '350',
                'description' => 'Sin poste letrero mayor a 15 pies de ancho'
            ],

            [
                'name' => 'Instalación E',
                'value' => '635',
                'description' => 'Sin poste letrero mayor a 15 pies de ancho'
            ],

        ]);


        /*
         * Papeleria
         */
        DB::table('options')->insert([
            [
                'name' => 'Cartulina perlada',
                'value' => 'Cartulina perlada',
                'description' => 'Material Uno Papeleria'
            ],

            [
                'name' => 'Cartulina satinada',
                'value' => 'Cartulina satinada',
                'description' => 'Material Uno Papeleria'
            ],

            [
                'name' => 'Cartulina espejo',
                'value' => 'Cartulina espejo',
                'description' => 'Material Uno Papeleria'
            ],

            [
                'name' => 'Papel satinado',
                'value' => 'Papel satinado',
                'description' => 'Material Uno Papeleria'
            ],

            [
                'name' => 'Sulfito',
                'value' => 'Sulfito',
                'description' => 'Material Uno Papeleria'
            ],

            [
                'name' => 'Una cara',
                'value' => 'Una cara',
                'description' => 'Caras Papeleria'
            ],

            [
                'name' => 'Dos caras',
                'value' => 'Dos caras',
                'description' => 'Caras Papeleria'
            ],

            [
                'name' => 'No lleva',
                'value' => 'No lleva',
                'description' => 'Laminado Papeleria'
            ],

            [
                'name' => 'Mate',
                'value' => 'Mate',
                'description' => 'Laminado Papeleria'
            ],

            [
                'name' => 'Brillante',
                'value' => 'Brillante',
                'description' => 'Laminado Papeleria'
            ],

            [
                'name' => 'Plastificado',
                'value' => 'Plastificado',
                'description' => 'Laminado Papeleria'
            ],


            [
                'name' => 'Papel adhesivo brillante',
                'value' => 'Papel adhesivo brillante',
                'description' => 'Material Dos Papeleria'
            ],

            [
                'name' => 'Papel adhesivo Mate',
                'value' => 'Papel adhesivo Mate',
                'description' => 'Material Dos Papeleria'
            ],

            [
                'name' => 'No lleva',
                'value' => 'No lleva',
                'description' => 'Troquelado Papeleria'
            ],

            [
                'name' => 'Troquelado',
                'value' => 'Troquelado',
                'description' => 'Troquelado Papeleria'
            ],


            [
                'name' => 'Cartulina perlada 1 - 50',
                'value' => '2.00',
                'description' => 'Una cara Papeleria'
            ],


            [
                'name' => 'Cartulina satinada 1 - 50',
                'value' => '1.00',
                'description' => 'Una cara Papeleria'
            ],


            [
                'name' => 'Cartulina espejo 1 - 50',
                'value' => '1.45',
                'description' => 'Una cara Papeleria'
            ],


            [
                'name' => 'Papel satinado 1 - 50',
                'value' => '0.85',
                'description' => 'Una cara Papeleria'
            ],


            [
                'name' => 'Sulfito 1 - 50',
                'value' => '0.95',
                'description' => 'Una cara Papeleria'
            ],


            [
                'name' => 'Plastificado 11.5" x 17.5" 1 - 50',
                'value' => '2.00',
                'description' => 'Una cara Papeleria'
            ],


            [
                'name' => 'Cartulina perlada 51 - 250',
                'value' => '1.90',
                'description' => 'Una cara Papeleria'
            ],


            [
                'name' => 'Cartulina satinada 51 - 250',
                'value' => '0.90',
                'description' => 'Una cara Papeleria'
            ],


            [
                'name' => 'Cartulina espejo 51 - 250',
                'value' => '1.25',
                'description' => 'Una cara Papeleria'
            ],


            [
                'name' => 'Papel satinado 51 - 250',
                'value' => '0.80',
                'description' => 'Una cara Papeleria'
            ],


            [
                'name' => 'Sulfito 51 - 250',
                'value' => '0.90',
                'description' => 'Una cara Papeleria'
            ],


            [
                'name' => 'Plastificado 11.5" x 17.5" 51 - 250',
                'value' => '1.85',
                'description' => 'Una cara Papeleria'
            ],


            [
                'name' => 'Cartulina perlada 251 - 500',
                'value' => '1.80',
                'description' => 'Una cara Papeleria'
            ],


            [
                'name' => 'Cartulina satinada 251 - 500',
                'value' => '0.80',
                'description' => 'Una cara Papeleria'
            ],


            [
                'name' => 'Cartulina espejo 251 - 500',
                'value' => '1.10',
                'description' => 'Una cara Papeleria'
            ],


            [
                'name' => 'Papel satinado 251 - 500',
                'value' => '0.75',
                'description' => 'Una cara Papeleria'
            ],


            [
                'name' => 'Sulfito 251 - 500',
                'value' => '0.85',
                'description' => 'Una cara Papeleria'
            ],


            [
                'name' => 'Plastificado 11.5" x 17.5" 251 - 500',
                'value' => '1.75',
                'description' => 'Una cara Papeleria'
            ],


            [
                'name' => 'Cartulina perlada 500+',
                'value' => '1.70',
                'description' => 'Una cara Papeleria'
            ],


            [
                'name' => 'Cartulina satinada 500+',
                'value' => '0.70',
                'description' => 'Una cara Papeleria'
            ],


            [
                'name' => 'Cartulina espejo 500+',
                'value' => '1.00',
                'description' => 'Una cara Papeleria'
            ],


            [
                'name' => 'Papel satinado 500+',
                'value' => '0.65',
                'description' => 'Una cara Papeleria'
            ],


            [
                'name' => 'Sulfito 500+',
                'value' => '0.75',
                'description' => 'Una cara Papeleria'
            ],


            [
                'name' => 'Plastificado 11.5" x 17.5" 500+',
                'value' => '1.65',
                'description' => 'Una cara Papeleria'
            ],


            [
                'name' => 'Cartulina perlada 1 - 50',
                'value' => '2.50',
                'description' => 'Dos caras Papeleria'
            ],


            [
                'name' => 'Cartulina satinada 1 - 50',
                'value' => '1.45',
                'description' => 'Dos caras Papeleria'
            ],


            [
                'name' => 'Cartulina espejo 1 - 50',
                'value' => '1.80',
                'description' => 'Dos caras Papeleria'
            ],


            [
                'name' => 'Papel satinado 1 - 50',
                'value' => '1.30',
                'description' => 'Dos caras Papeleria'
            ],


            [
                'name' => 'Sulfito 1 - 50',
                'value' => '1.40',
                'description' => 'Dos caras Papeleria'
            ],


            [
                'name' => 'Plastificado 11.5" x 17.5" 1 - 50',
                'value' => '2.00',
                'description' => 'Dos caras Papeleria'
            ],


            [
                'name' => 'Cartulina perlada 51 - 250',
                'value' => '2.30',
                'description' => 'Dos caras Papeleria'
            ],


            [
                'name' => 'Cartulina satinada 51 - 250',
                'value' => '1.30',
                'description' => 'Dos caras Papeleria'
            ],


            [
                'name' => 'Cartulina espejo 51 - 250',
                'value' => '1.65',
                'description' => 'Dos caras Papeleria'
            ],


            [
                'name' => 'Papel satinado 51 - 250',
                'value' => '1.20',
                'description' => 'Dos caras Papeleria'
            ],


            [
                'name' => 'Sulfito 51 - 250',
                'value' => '1.30',
                'description' => 'Dos caras Papeleria'
            ],


            [
                'name' => 'Plastificado 11.5" x 17.5" 51 - 250',
                'value' => '1.85',
                'description' => 'Dos caras Papeleria'
            ],

            [
                'name' => 'Cartulina perlada 251 - 500',
                'value' => '2.05',
                'description' => 'Dos caras Papeleria'
            ],


            [
                'name' => 'Cartulina satinada 251 - 500',
                'value' => '1.15',
                'description' => 'Dos caras Papeleria'
            ],


            [
                'name' => 'Cartulina espejo 251 - 500',
                'value' => '1.50',
                'description' => 'Dos caras Papeleria'
            ],


            [
                'name' => 'Papel satinado 251 - 500',
                'value' => '1.05',
                'description' => 'Dos caras Papeleria'
            ],


            [
                'name' => 'Sulfito 251 - 500',
                'value' => '1.15',
                'description' => 'Dos caras Papeleria'
            ],


            [
                'name' => 'Plastificado 11.5" x 17.5" 251 - 500',
                'value' => '1.75',
                'description' => 'Dos caras Papeleria'
            ],


            [
                'name' => 'Cartulina perlada 500+',
                'value' => '1.80',
                'description' => 'Dos caras Papeleria'
            ],


            [
                'name' => 'Cartulina satinada 500+',
                'value' => '0.85',
                'description' => 'Dos caras Papeleria'
            ],


            [
                'name' => 'Cartulina espejo 500+',
                'value' => '1.25',
                'description' => 'Dos caras Papeleria'
            ],


            [
                'name' => 'Papel satinado 500+',
                'value' => '0.90',
                'description' => 'Dos caras Papeleria'
            ],


            [
                'name' => 'Sulfito 500+',
                'value' => '0.95',
                'description' => 'Dos caras Papeleria'
            ],


            [
                'name' => 'Plastificado 11.5" x 17.5" 500+',
                'value' => '1.65',
                'description' => 'Dos caras Papeleria'
            ],


            [
                'name' => 'Rollo de laminado mate 25" x 500 pies',
                'value' => '3.00',
                'description' => ' 12 * 18 Material Laminado Precio Papeleria'
            ],

            [
                'name' => 'Rollo de laminado brillante 25" x 500 pies',
                'value' => '2.50',
                'description' => ' 12 * 18 Material Laminado Precio Papeleria'
            ],

            [
                'name' => 'Plastificado 11.5 x 17.5 pulgadas, 10mil micrones',
                'value' => '3',
                'description' => ' 12 * 18 Material Laminado Precio Papeleria'
            ],

            [
                'name' => 'Papel adhesivo brillante',
                'value' => '1.85',
                'description' => 'Precio Ahesivos Material'
            ],

            [
                'name' => 'Papel adhesivo Mate',
                'value' => '1.75',
                'description' => 'Precio Ahesivos Material'
            ],

            [
                'name' => 'Papel adhesivo brillante Troquelado',
                'value' => '4.00',
                'description' => 'Precio Troquelado 1 - 50'
            ],

            [
                'name' => 'Papel adhesivo Mate Troquelado',
                'value' => '4.00',
                'description' => 'Precio Troquelado 1 - 50'
            ],

            [
                'name' => 'Papel adhesivo brillante Troquelado',
                'value' => '2.00',
                'description' => 'Precio Troquelado 50 - 500'
            ],

            [
                'name' => 'Papel adhesivo Mate Troquelado',
                'value' => '2.00',
                'description' => 'Precio Troquelado 50 - 500'
            ],


        ]);


        /*
         * Impresion
         */
        DB::table('options')->insert([
            [
                'name' => 'Banner Excelsys de 13 oz.',
                'value' => 'Banner Excelsys de 13 oz.',
                'description' => 'Material Impresion'
            ],

            [
                'name' => 'Lona Translucida Excelsys  20 oz.',
                'value' => 'Lona Translucida Excelsys  20 oz.',
                'description' => 'Material Impresion'
            ],

            [
                'name' => 'Lona Translucida LG',
                'value' => 'Lona Translucida LG',
                'description' => 'Material Impresion'
            ],

            [
                'name' => 'Mesh',
                'value' => 'Mesh',
                'description' => 'Material Impresion'
            ],

            [
                'name' => 'Vinil Generico',
                'value' => 'Vinil Generico',
                'description' => 'Material Impresion'
            ],

            [
                'name' => 'Vinil AVERY removible ',
                'value' => 'Vinil AVERY removible ',
                'description' => 'Material Impresion'
            ],

            [
                'name' => 'Vinil LG ',
                'value' => 'Vinil LG ',
                'description' => 'Material Impresion'
            ],


            [
                'name' => 'Vinil Controltac 3M',
                'value' => 'Vinil Controltac 3M',
                'description' => 'Material Impresion'
            ],

            [
                'name' => 'Micro Perforado',
                'value' => 'Micro Perforado',
                'description' => 'Material Impresion'
            ],

            [
                'name' => 'Canva',
                'value' => 'Canva',
                'description' => 'Material Impresion'
            ],


            [
                'name' => 'Papel Bond',
                'value' => 'Papel Bond',
                'description' => 'Material Impresion'
            ],

            [
                'name' => 'Papel Fotográfico',
                'value' => 'Papel Fotográfico',
                'description' => 'Material Impresion'
            ],


            [
                'name' => 'Tela para Banderolas',
                'value' => 'Tela para Banderolas',
                'description' => 'Material Impresion'
            ],

            [
                'name' => 'Duratrans',
                'value' => 'Duratrans',
                'description' => 'Material Impresion'
            ],

            [
                'name' => 'Times',
                'value' => 'Times',
                'description' => 'Equipo Impresion'
            ],


            [
                'name' => 'Mimaki',
                'value' => 'Mimaki',
                'description' => 'Equipo Impresion'
            ],

            [
                'name' => 'DGI',
                'value' => 'DGI',
                'description' => 'Equipo Impresion'
            ],

            [
                'name' => 'Ricoh',
                'value' => 'Ricoh',
                'description' => 'Equipo Impresion'
            ],

            [
                'name' => 'Banner Excelsys de 13 oz.',
                'value' => '1.25',
                'description' => 'Material Times Precio Impresion'
            ],

            [
                'name' => 'Lona Translucida Excelsys  20 oz.',
                'value' => '3.00',
                'description' => 'Material Times Precio Impresion'
            ],

            [
                'name' => 'Lona Translucida LG',
                'value' => '4.00',
                'description' => 'Material Times Precio Impresion'
            ],

            [
                'name' => 'Mesh',
                'value' => '2.00',
                'description' => 'Material Times Precio Impresion'
            ],

            [
                'name' => 'Vinil Generico',
                'value' => '1.00',
                'description' => 'Material Times Precio Impresion'
            ],

            [
                'name' => 'Vinil AVERY removible',
                'value' => '1.50',
                'description' => 'Material Times Precio Impresion'
            ],

            [
                'name' => 'Vinil LG',
                'value' => '1.30',
                'description' => 'Material Times Precio Impresion'
            ],

            [
                'name' => 'Vinil Controltac 3M',
                'value' => '4.15',
                'description' => 'Material Times Precio Impresion'
            ],

            [
                'name' => 'Micro Perforado',
                'value' => '2.00',
                'description' => 'Material Times Precio Impresion'
            ],

            [
                'name' => 'Canva',
                'value' => '1.70',
                'description' => 'Material Times Precio Impresion'
            ],

            [
                'name' => 'Papel Bond',
                'value' => '0.70',
                'description' => 'Material Times Precio Impresion'
            ],


            [
                'name' => 'Papel Fotográfico',
                'value' => '1.35',
                'description' => 'Material Times Precio Impresion'
            ],

            [
                'name' => 'Tela para Banderolas',
                'value' => '2.45',
                'description' => 'Material Times Precio Impresion'
            ],

            [
                'name' => 'Duratrans',
                'value' => '2.50',
                'description' => 'Material Times Precio Impresion'
            ],


            [
                'name' => 'Banner Excelsys de 13 oz.',
                'value' => '1.50',
                'description' => 'Material Mimaki Precio Impresion'
            ],

            [
                'name' => 'Lona Translucida Excelsys  20 oz.',
                'value' => '3.50',
                'description' => 'Material Mimaki Precio Impresion'
            ],

            [
                'name' => 'Lona Translucida LG',
                'value' => '4.50',
                'description' => 'Material Mimaki Precio Impresion'
            ],

            [
                'name' => 'Mesh',
                'value' => '2.50',
                'description' => 'Material Mimaki Precio Impresion'
            ],

            [
                'name' => 'Vinil Generico',
                'value' => '1.50',
                'description' => 'Material Mimaki Precio Impresion'
            ],

            [
                'name' => 'Vinil AVERY removible',
                'value' => '1.85',
                'description' => 'Material Mimaki Precio Impresion'
            ],

            [
                'name' => 'Vinil LG',
                'value' => '1.50',
                'description' => 'Material Mimaki Precio Impresion'
            ],

            [
                'name' => 'Vinil Controltac 3M',
                'value' => '4.50',
                'description' => 'Material Mimaki Precio Impresion'
            ],

            [
                'name' => 'Micro Perforado',
                'value' => '2.75',
                'description' => 'Material Mimaki Precio Impresion'
            ],

            [
                'name' => 'Canva',
                'value' => '2.05',
                'description' => 'Material Mimaki Precio Impresion'
            ],

            [
                'name' => 'Papel Bond',
                'value' => '1.10',
                'description' => 'Material Mimaki Precio Impresion'
            ],


            [
                'name' => 'Papel Fotográfico',
                'value' => '1.70',
                'description' => 'Material Mimaki Precio Impresion'
            ],

            [
                'name' => 'Tela para Banderolas',
                'value' => '3.25',
                'description' => 'Material Mimaki Precio Impresion'
            ],

            [
                'name' => 'Duratrans',
                'value' => '3.00',
                'description' => 'Material Mimaki Precio Impresion'
            ],


            [
                'name' => 'Banner Excelsys de 13 oz.',
                'value' => '1.00',
                'description' => 'Material DGI Precio Impresion'
            ],

            [
                'name' => 'Lona Translucida Excelsys  20 oz.',
                'value' => '3.00',
                'description' => 'Material DGI Precio Impresion'
            ],

            [
                'name' => 'Lona Translucida LG',
                'value' => '4.00',
                'description' => 'Material DGI Precio Impresion'
            ],

            [
                'name' => 'Mesh',
                'value' => '1.50',
                'description' => 'Material DGI Precio Impresion'
            ],

            [
                'name' => 'Vinil Generico',
                'value' => '0.80',
                'description' => 'Material DGI Precio Impresion'
            ],

            [
                'name' => 'Vinil AVERY removible',
                'value' => '1.00',
                'description' => 'Material DGI Precio Impresion'
            ],

            [
                'name' => 'Vinil LG',
                'value' => '1.00',
                'description' => 'Material DGI Precio Impresion'
            ],

            [
                'name' => 'Vinil Controltac 3M',
                'value' => '3.90',
                'description' => 'Material DGI Precio Impresion'
            ],

            [
                'name' => 'Micro Perforado',
                'value' => '1.50',
                'description' => 'Material DGI Precio Impresion'
            ],

            [
                'name' => 'Canva',
                'value' => '1.45',
                'description' => 'Material DGI Precio Impresion'
            ],

            [
                'name' => 'Papel Bond',
                'value' => '0.50',
                'description' => 'Material DGI Precio Impresion'
            ],


            [
                'name' => 'Papel Fotográfico',
                'value' => '1.01',
                'description' => 'Material DGI Precio Impresion'
            ],

            [
                'name' => 'Tela para Banderolas',
                'value' => '2.00',
                'description' => 'Material DGI Precio Impresion'
            ],

            [
                'name' => 'Duratrans',
                'value' => '1.85',
                'description' => 'Material DGI Precio Impresion'
            ],

            /*
             *
             */

            [
                'name' => 'Banner Excelsys de 13 oz.',
                'value' => '2.00',
                'description' => 'Material Ricoh Precio Impresion'
            ],

            [
                'name' => 'Lona Translucida Excelsys  20 oz.',
                'value' => '4.00',
                'description' => 'Material Ricoh Precio Impresion'
            ],

            [
                'name' => 'Lona Translucida LG',
                'value' => '5.00',
                'description' => 'Material Ricoh Precio Impresion'
            ],

            [
                'name' => 'Mesh',
                'value' => '3.00',
                'description' => 'Material Ricoh Precio Impresion'
            ],

            [
                'name' => 'Vinil Generico',
                'value' => '2.00',
                'description' => 'Material Ricoh Precio Impresion'
            ],

            [
                'name' => 'Vinil AVERY removible',
                'value' => '2.00',
                'description' => 'Material Ricoh Precio Impresion'
            ],

            [
                'name' => 'Vinil LG',
                'value' => '1.90',
                'description' => 'Material Ricoh Precio Impresion'
            ],

            [
                'name' => 'Vinil Controltac 3M',
                'value' => '4.90',
                'description' => 'Material Ricoh Precio Impresion'
            ],

            [
                'name' => 'Micro Perforado',
                'value' => '3.00',
                'description' => 'Material Ricoh Precio Impresion'
            ],

            [
                'name' => 'Canva',
                'value' => '2.50',
                'description' => 'Material Ricoh Precio Impresion'
            ],

            [
                'name' => 'Papel Bond',
                'value' => '1.35',
                'description' => 'Material Ricoh Precio Impresion'
            ],


            [
                'name' => 'Papel Fotográfico',
                'value' => '2.00',
                'description' => 'Material Ricoh Precio Impresion'
            ],

            [
                'name' => 'Tela para Banderolas',
                'value' => '3.50',
                'description' => 'Material Ricoh Precio Impresion'
            ],

            [
                'name' => 'Duratrans',
                'value' => '3.50',
                'description' => 'Material Ricoh Precio Impresion'
            ],


            [
                'name' => 'Sin Laminado',
                'value' => 'Sin Laminado',
                'description' => 'Laminados Pies Cuadrados Impresion'
            ],

            [
                'name' => 'Laminado 8519  5 años 3 M',
                'value' => 'Laminado 8519  5 años 3 M',
                'description' => 'Laminados Pies Cuadrados Impresion'
            ],

            [
                'name' => 'Laminado de 3 años LG',
                'value' => 'Laminado de 3 años LG',
                'description' => 'Laminados Pies Cuadrados Impresion'
            ],

            [
                'name' => 'Laminado de piso floorgraphic',
                'value' => 'Laminado de piso floorgraphic',
                'description' => 'Laminados Pies Cuadrados Impresion'
            ],

        ]);


        /*
         * Facias
         */
        DB::table('options')->insert([

            [
                'name' => 'Aluminio',
                'value' => 'Aluminio',
                'description' => 'Material Facias'
            ],

            [
                'name' => 'Hierro',
                'value' => 'Hierro',
                'description' => 'Material Facias'
            ],

            [
                'name' => 'A',
                'value' => '45',
                'description' => 'Sector menos de 15 pies Facias'
            ],

            [
                'name' => 'B',
                'value' => '120',
                'description' => 'Sector menos de 15 pies Facias'
            ],


            [
                'name' => 'C',
                'value' => '160',
                'description' => 'Sector menos de 15 pies Facias'
            ],

            [
                'name' => 'D',
                'value' => '280',
                'description' => 'Sector menos de 15 pies Facias'
            ],

            [
                'name' => 'E',
                'value' => '550',
                'description' => 'Sector menos de 15 pies Facias'
            ],

            [
                'name' => 'A',
                'value' => '65',
                'description' => 'Sector entre 15 y 40 pies Facias'
            ],

            [
                'name' => 'B',
                'value' => '160',
                'description' => 'Sector entre 15 y 40 pies Facias'
            ],

            [
                'name' => 'C',
                'value' => '200',
                'description' => 'Sector entre 15 y 40 pies Facias'
            ],

            [
                'name' => 'D',
                'value' => '350',
                'description' => 'Sector entre 15 y 40 pies Facias'
            ],

            [
                'name' => 'E',
                'value' => '635',
                'description' => 'Sector entre 15 y 40 pies Facias'
            ],

            [
                'name' => 'A',
                'value' => '75',
                'description' => 'Sector más de 40 pies Facias'
            ],

            [
                'name' => 'B',
                'value' => '184',
                'description' => 'Sector más de 40 pies Facias'
            ],

            [
                'name' => 'C',
                'value' => '230',
                'description' => 'Sector más de 40 pies Facias'
            ],

            [
                'name' => 'D',
                'value' => '403',
                'description' => 'Sector más de 40 pies Facias'
            ],

            [
                'name' => 'E',
                'value' => '730',
                'description' => 'Sector más de 40 pies Facias'
            ],

            [
                'name' => 'estructura',
                'value' => '2.00',
                'description' => 'Precio mas 20 pies en aluminio Facias'
            ],

            [
                'name' => 'impresion',
                'value' => '1.50',
                'description' => 'Precio mas 20 pies en aluminio Facias'
            ],

            [
                'name' => 'METAL lata',
                'value' => '1.25',
                'description' => 'Precio mas 20 pies en aluminio Facias'
            ],

            [
                'name' => 'laminado',
                'value' => '1.35',
                'description' => 'Precio mas 20 pies en aluminio Facias'
            ],

            /*
             *
             */

            [
                'name' => 'estructura',
                'value' => '1.35',
                'description' => 'Precio mas de 20 pies en hierro Facias'
            ],

            [
                'name' => 'impresion',
                'value' => '1.50',
                'description' => 'Precio mas de 20 pies en hierro Facias'
            ],

            [
                'name' => 'METAL lata',
                'value' => '1.25',
                'description' => 'Precio mas de 20 pies en hierro Facias'
            ],

            [
                'name' => 'laminado',
                'value' => '1.35',
                'description' => 'Precio mas de 20 pies en hierro Facias'
            ],

            /*
             *
             */
            [
                'name' => 'estructura',
                'value' => '2.50',
                'description' => 'Precio menos de 20 en aluminio Facias'
            ],

            [
                'name' => 'impresion',
                'value' => '1.50',
                'description' => 'Precio menos de 20 en aluminio Facias'
            ],

            [
                'name' => 'METAL lata',
                'value' => '1.25',
                'description' => 'Precio menos de 20 en aluminio Facias'
            ],

            [
                'name' => 'laminado',
                'value' => '1.35',
                'description' => 'Precio menos de 20 en aluminio Facias'
            ],


            [
                'name' => 'estructura',
                'value' => '1.75',
                'description' => 'Precio menos de 20 pies en hierro Facias'
            ],

            [
                'name' => 'impresion',
                'value' => '1.50',
                'description' => 'Precio menos de 20 pies en hierro Facias'
            ],

            [
                'name' => 'METAL lata',
                'value' => '1.25',
                'description' => 'Precio menos de 20 pies en hierro Facias'
            ],

            [
                'name' => 'laminado',
                'value' => '1.35',
                'description' => 'Precio menos de 20 pies en hierro Facias'
            ],

            [
                'name' => 'Sin Laminado',
                'value' => '0.00',
                'description' => 'Laminados Pies Cuadrados Impresion'
            ],

            [
                'name' => 'Laminado 8519  5 años 3 M',
                'value' => '3.00',
                'description' => 'Laminados Pies Cuadrados Impresion'
            ],

            [
                'name' => 'Laminado de 3 años LG',
                'value' => '1.50',
                'description' => 'Laminados Pies Cuadrados Impresion'
            ],

            [
                'name' => 'Laminado de piso floorgraphic',
                'value' => '1.50',
                'description' => 'Laminados Pies Cuadrados Impresion'
            ],


        ]);


        /*
         * Corte Sector
         */
        DB::table('options')->insert([
            [
                'name' => 'Chitré',
                'value' => 'Chitré',
                'description' => 'Localización'
            ],

            [
                'name' => 'Penonomé',
                'value' => 'Penonomé',
                'description' => 'Localización'
            ],

            [
                'name' => 'Coronado',
                'value' => 'Coronado',
                'description' => 'Localización'
            ],
            [
                'name' => 'Panamá',
                'value' => 'Panamá',
                'description' => 'Localización'
            ],

            [
                'name' => 'Bocas del Toro',
                'value' => 'Bocas del Toro',
                'description' => 'Localización'
            ],

            [
                'name' => 'Las tablas',
                'value' => 'Las tablas',
                'description' => 'Localización'
            ],

            [
                'name' => '',
                'value' => '',
                'description' => 'Localización'
            ],

            [
                'name' => 'Las Minas',
                'value' => 'Las Minas',
                'description' => 'Localización'
            ],

            [
                'name' => 'Tonosí',
                'value' => 'Tonosí',
                'description' => 'Localización'
            ],

            [
                'name' => 'Chorrera',
                'value' => 'Chorrera',
                'description' => 'Localización'
            ],

            [
                'name' => 'Darién',
                'value' => 'Darién',
                'description' => 'Localización'
            ],

            [
                'name' => 'Pese',
                'value' => 'Pese',
                'description' => 'Localización'
            ],

            [
                'name' => 'Parita',
                'value' => 'Parita',
                'description' => 'Localización'
            ],

            [
                'name' => 'Los Pozos',
                'value' => 'Los Pozos',
                'description' => 'Localización'
            ],

            [
                'name' => 'Rio Hato',
                'value' => 'Rio Hato',
                'description' => 'Localización'
            ],

            [
                'name' => 'Chiriquí',
                'value' => 'Chiriquí',
                'description' => 'Localización'
            ],

            [
                'name' => '',
                'value' => '',
                'description' => 'Localización vacía'
            ],

            [
                'name' => 'Guararé',
                'value' => 'Guararé',
                'description' => 'Localización'
            ],


            [
                'name' => 'Los Santos',
                'value' => 'Los Santos',
                'description' => 'Localización'
            ],


            [
                'name' => 'Santiago',
                'value' => 'Santiago',
                'description' => 'Localización'
            ],

            [
                'name' => 'Antón',
                'value' => 'Antón',
                'description' => 'Localización'
            ],

            [
                'name' => 'Colón',
                'value' => 'Colón',
                'description' => 'Localización'
            ],

            [
                'name' => '',
                'value' => '',
                'description' => 'Localización vacía'
            ],

            [
                'name' => '',
                'value' => '',
                'description' => 'Localización vacía'
            ],

            [
                'name' => 'Monagrillo',
                'value' => 'Monagrillo',
                'description' => 'Localización'
            ],

            [
                'name' => 'Pedasí',
                'value' => 'Pedasí',
                'description' => 'Localización'
            ],

            [
                'name' => '',
                'value' => '',
                'description' => 'Localización vacía'
            ],

            [
                'name' => '',
                'value' => '',
                'description' => 'Localización vacía'
            ],

            [
                'name' => '',
                'value' => '',
                'description' => 'Localización vacía'
            ],

            [
                'name' => '',
                'value' => '',
                'description' => 'Localización vacía'
            ],

            [
                'name' => 'La Arena',
                'value' => 'La Arena',
                'description' => 'Localización'
            ],

            [
                'name' => 'El Roble',
                'value' => 'El Roble',
                'description' => 'Localización'
            ],

            [
                'name' => '',
                'value' => '',
                'description' => 'Localización vacía'
            ],

            [
                'name' => '',
                'value' => '',
                'description' => 'Localización vacía'
            ],

            [
                'name' => '',
                'value' => '',
                'description' => 'Localización vacía'
            ],


            [
                'name' => '',
                'value' => '',
                'description' => 'Localización vacía'
            ],


            [
                'name' => '',
                'value' => '',
                'description' => 'Localización vacía'
            ],


            [
                'name' => 'Macaracas',
                'value' => 'Macaracas',
                'description' => 'Localización'
            ],

            [
                'name' => '',
                'value' => '',
                'description' => 'Localización vacía'
            ],

            [
                'name' => '',
                'value' => '',
                'description' => 'Localización vacía'
            ],

            [
                'name' => '',
                'value' => '',
                'description' => 'Localización vacía'
            ],

            [
                'name' => '',
                'value' => '',
                'description' => 'Localización vacía'
            ],

            [
                'name' => '',
                'value' => '',
                'description' => 'Localización vacía'
            ],

            [
                'name' => 'Aguadulce',
                'value' => 'Aguadulce',
                'description' => 'Localización'
            ],

            [
                'name' => '',
                'value' => '',
                'description' => 'Localización vacía'
            ],

            [
                'name' => '',
                'value' => '',
                'description' => 'Localización vacía'
            ],

            [
                'name' => '',
                'value' => '',
                'description' => 'Localización vacía'
            ],

            [
                'name' => '',
                'value' => '',
                'description' => 'Localización vacía'
            ],

            [
                'name' => 'A',
                'value' => '15',
                'description' => 'Sector corte price'
            ],

            [
                'name' => 'B',
                'value' => '100',
                'description' => 'Sector corte price'
            ],

            [
                'name' => 'C',
                'value' => '130',
                'description' => 'Sector corte price'
            ],

            [
                'name' => 'D',
                'value' => '230',
                'description' => 'Sector corte price'
            ],

            [
                'name' => 'E',
                'value' => '500',
                'description' => 'Sector corte price'
            ],

            [
                'name' => 'F',
                'value' => '40',
                'description' => 'Sector corte price'
            ],


        ]);

        /*
         * Toldas and Carpas
         */
        DB::table('item_options')->insert([
            [
                'item' => 2,
                'option' => 1
            ],
            [
                'item' => 2,
                'option' => 2
            ],

            [
                'item' => 2,
                'option' => 3
            ],

            [
                'item' => 1,
                'option' => 4
            ],
            [
                'item' => 1,
                'option' => 5
            ],

            [
                'item' => 1,
                'option' => 6
            ],

            [
                'item' => 1,
                'option' => 7
            ],
        ]);

        DB::table('options')->insert([
            [
                'name' => 'CONFECCIÓN DE ESTANDARTE 2 X3 SATIN',
                'value' => '130',
                'description' => 'Impresion estandarizada'
            ],

            [
                'name' => 'CONFECCIÓN DE ESTANDARTE 3 X 4 SATIN',
                'value' => '150',
                'description' => 'Impresion estandarizada'
            ],

            [
                'name' => 'ROLL UP CON IMPRESIÓN BANNER',
                'value' => '65',
                'description' => 'Impresion estandarizada'
            ],

            [
                'name' => 'ROLL UP CON PAPEL FOTOGRAFICO',
                'value' => '80',
                'description' => 'Impresion estandarizada'
            ],

            [
                'name' => 'ARAÑITA CON BANNER 2X5 PIES',
                'value' => '45',
                'description' => 'Impresion estandarizada'
            ],

            [
                'name' => 'SOLAMENTE IMPRESIÒN DE ESTANDARTE 2X4 PIES',
                'value' => '30',
                'description' => 'Impresion estandarizada'
            ],


            [
                'name' => 'SOLAMENTE IMPRESIÒN DE ESTANDARTE 2X3 PIES',
                'value' => '54',
                'description' => 'Impresion estandarizada'
            ],
        ]);

        DB::table('options')->insert([
            [
                'name' => 'ROMPECABEZA MAGNETICO',
                'value' => 'ROMPECABEZA MAGNETICO',
                'description' => 'Suvenir material'
            ],

            [
                'name' => 'ROMPECABEZA RECTANGULAR',
                'value' => 'ROMPECABEZA RECTANGULAR',
                'description' => 'Suvenir material'
            ],

            [
                'name' => 'ROMPECABEZA CORAZON',
                'value' => 'ROMPECABEZA CORAZON',
                'description' => 'Suvenir material'
            ],

            [
                'name' => 'ROMPECABEZA POLYMER',
                'value' => 'ROMPECABEZA POLYMER',
                'description' => 'Suvenir material'
            ],

            [
                'name' => 'MOUSE PAD',
                'value' => 'MOUSE PAD',
                'description' => 'Suvenir material'
            ],

            [
                'name' => 'TAZA BLANCO',
                'value' => 'TAZA BLANCO',
                'description' => 'Suvenir material'
            ],

            [
                'name' => 'TAZA DE COLORES',
                'value' => 'TAZA DE COLORES',
                'description' => 'Suvenir material'
            ],

            [
                'name' => 'TAZAS MAGICAS',
                'value' => 'TAZAS MAGICAS',
                'description' => 'Suvenir material'
            ],

            [
                'name' => 'TAZA EXPRESO',
                'value' => 'TAZA EXPRESO',
                'description' => 'Suvenir material'
            ],

            [
                'name' => 'JARRA SUBLIMADA CON AGARADERO',
                'value' => 'JARRA SUBLIMADA CON AGARADERO',
                'description' => 'Suvenir material'
            ],


            [
                'name' => 'BOTELLA DEPORTIVA',
                'value' => 'BOTELLA DEPORTIVA',
                'description' => 'Suvenir material'
            ],


            [
                'name' => 'BOTELLA DEPORTIVA TAPA DE PRENSA',
                'value' => 'BOTELLA DEPORTIVA TAPA DE PRENSA',
                'description' => 'Suvenir material'
            ],


            [
                'name' => 'BOTELLA DEPORTIVA PLATEADO',
                'value' => 'BOTELLA DEPORTIVA PLATEADO',
                'description' => 'Suvenir material'
            ],


            [
                'name' => 'BOLSO CON CORDON NEGRO',
                'value' => 'BOLSO CON CORDON NEGRO',
                'description' => 'Suvenir material'
            ],


            [
                'name' => 'LLAVERO',
                'value' => 'LLAVERO',
                'description' => 'Suvenir material'
            ],


            [
                'name' => 'PLATOS GRANDES',
                'value' => 'PLATOS GRANDES',
                'description' => 'Suvenir material'
            ],


            [
                'name' => 'PLACA DE RECONOCIMIENTO',
                'value' => 'PLACA DE RECONOCIMIENTO',
                'description' => 'Suvenir material'
            ],


            [
                'name' => 'PLATOS CHICOS',
                'value' => 'PLATOS CHICOS',
                'description' => 'Suvenir material'
            ],


            [
                'name' => 'BOLSO NYLON',
                'value' => 'BOLSO NYLON',
                'description' => 'Suvenir material'
            ],


            [
                'name' => 'BOLSO POLYESTER 35X37CM',
                'value' => 'BOLSO POLYESTER 35X37CM',
                'description' => 'Suvenir material'
            ],


            [
                'name' => 'BOLSO POLYESTER 19.5X30CM',
                'value' => 'BOLSO POLYESTER 19.5X30CM',
                'description' => 'Suvenir material'
            ],
        ]);

        DB::table('options')->insert([
            [
                'name' => 'ROMPECABEZA MAGNETICO',
                'value' => '5.00',
                'description' => 'Suvenir material precio'
            ],

            [
                'name' => 'ROMPECABEZA RECTANGULAR',
                'value' => '3.50',
                'description' => 'Suvenir material precio'
            ],

            [
                'name' => 'ROMPECABEZA CORAZON',
                'value' => '3.25',
                'description' => 'Suvenir material precio'
            ],

            [
                'name' => 'ROMPECABEZA POLYMER',
                'value' => '6.00',
                'description' => 'Suvenir material precio'
            ],

            [
                'name' => 'MOUSE PAD',
                'value' => '3.00',
                'description' => 'Suvenir material precio'
            ],

            [
                'name' => 'TAZA BLANCO',
                'value' => '5.00',
                'description' => 'Suvenir material precio'
            ],

            [
                'name' => 'TAZA DE COLORES',
                'value' => '6.00',
                'description' => 'Suvenir material precio'
            ],

            [
                'name' => 'TAZAS MAGICAS',
                'value' => '8.00',
                'description' => 'Suvenir material precio'
            ],

            [
                'name' => 'TAZA EXPRESO',
                'value' => '6.50',
                'description' => 'Suvenir material precio'
            ],

            [
                'name' => 'JARRA SUBLIMADA CON AGARADERO',
                'value' => '10.00',
                'description' => 'Suvenir material precio'
            ],


            [
                'name' => 'BOTELLA DEPORTIVA',
                'value' => '9.50',
                'description' => 'Suvenir material precio'
            ],


            [
                'name' => 'BOTELLA DEPORTIVA TAPA DE PRENSA',
                'value' => '9.50',
                'description' => 'Suvenir material precio'
            ],


            [
                'name' => 'BOTELLA DEPORTIVA PLATEADO',
                'value' => '6.50',
                'description' => 'Suvenir material precio'
            ],


            [
                'name' => 'BOLSO CON CORDON NEGRO',
                'value' => '10.00',
                'description' => 'Suvenir material precio'
            ],


            [
                'name' => 'LLAVERO',
                'value' => '2.50',
                'description' => 'Suvenir material precio'
            ],


            [
                'name' => 'PLATOS GRANDES',
                'value' => '10.00',
                'description' => 'Suvenir material precio'
            ],


            [
                'name' => 'PLACA DE RECONOCIMIENTO',
                'value' => '35.00',
                'description' => 'Suvenir material precio'
            ],


            [
                'name' => 'PLATOS CHICOS',
                'value' => '8.00',
                'description' => 'Suvenir material precio'
            ],


            [
                'name' => 'BOLSO NYLON',
                'value' => '13.00',
                'description' => 'Suvenir material precio'
            ],


            [
                'name' => 'BOLSO POLYESTER 35X37CM',
                'value' => '3.00',
                'description' => 'Suvenir material precio'
            ],


            [
                'name' => 'BOLSO POLYESTER 19.5X30CM',
                'value' => '2.50',
                'description' => 'Suvenir material precio'
            ],
        ]);


        DB::table('options')->insert([
            [
                'name' => 'ROMPECABEZA MAGNETICO',
                'value' => '3.35',
                'description' => 'Suvenir material precio 12 unidades'
            ],

            [
                'name' => 'ROMPECABEZA RECTANGULAR',
                'value' => '2.50',
                'description' => 'Suvenir material precio 12 unidades'
            ],

            [
                'name' => 'ROMPECABEZA CORAZON',
                'value' => '2.28',
                'description' => 'Suvenir material precio 12 unidades'
            ],

            [
                'name' => 'ROMPECABEZA POLYMER',
                'value' => '4.25',
                'description' => 'Suvenir material precio 12 unidades'
            ],

            [
                'name' => 'MOUSE PAD',
                'value' => '2.25',
                'description' => 'Suvenir material precio 12 unidades'
            ],

            [
                'name' => 'TAZA BLANCO',
                'value' => '3.00',
                'description' => 'Suvenir material precio 12 unidades'
            ],

            [
                'name' => 'TAZA DE COLORES',
                'value' => '3.85',
                'description' => 'Suvenir material precio 12 unidades'
            ],

            [
                'name' => 'TAZAS MAGICAS',
                'value' => '6.00',
                'description' => 'Suvenir material precio 12 unidades'
            ],

            [
                'name' => 'TAZA EXPRESO',
                'value' => '3.45',
                'description' => 'Suvenir material precio 12 unidades'
            ],

            [
                'name' => 'JARRA SUBLIMADA CON AGARADERO',
                'value' => '8.35',
                'description' => 'Suvenir material precio 12 unidades'
            ],


            [
                'name' => 'BOTELLA DEPORTIVA',
                'value' => '8.15',
                'description' => 'Suvenir material precio 12 unidades'
            ],


            [
                'name' => 'BOTELLA DEPORTIVA TAPA DE PRENSA',
                'value' => '8.15',
                'description' => 'Suvenir material precio 12 unidades'
            ],


            [
                'name' => 'BOTELLA DEPORTIVA PLATEADO',
                'value' => '5.50',
                'description' => 'Suvenir material precio 12 unidades'
            ],


            [
                'name' => 'BOLSO CON CORDON NEGRO',
                'value' => '8.15',
                'description' => 'Suvenir material precio 12 unidades'
            ],


            [
                'name' => 'LLAVERO',
                'value' => '2.50',
                'description' => 'Suvenir material precio 12 unidades'
            ],


            [
                'name' => 'PLATOS GRANDES',
                'value' => '8.00',
                'description' => 'Suvenir material precio 12 unidades'
            ],


            [
                'name' => 'PLACA DE RECONOCIMIENTO',
                'value' => '30.00',
                'description' => 'Suvenir material precio 12 unidades'
            ],


            [
                'name' => 'PLATOS CHICOS',
                'value' => '6.00',
                'description' => 'Suvenir material precio 12 unidades'
            ],


            [
                'name' => 'BOLSO NYLON',
                'value' => '10.35',
                'description' => 'Suvenir material precio 12 unidades'
            ],


            [
                'name' => 'BOLSO POLYESTER 35X37CM',
                'value' => '2.15',
                'description' => 'Suvenir material precio 12 unidades'
            ],

            [
                'name' => 'BOLSO POLYESTER 19.5X30CM',
                'value' => '2.00',
                'description' => 'Suvenir material precio 12 unidades'
            ],
        ]);


        DB::table('options')->insert([
            [
                'name' => 'BOTONES',
                'value' => 'BOTONES',
                'description' => 'Otros Material'
            ],

            [
                'name' => 'GAFETES  3"X1" O 3"X.75',
                'value' => 'GAFETES  3"X1" O 3"X.75',
                'description' => 'Otros Material'
            ],

            [
                'name' => 'HABLADOR 8.5" X 11"',
                'value' => 'HABLADOR 8.5" X 11"',
                'description' => 'Otros Material'
            ],

            [
                'name' => 'PORTA BROCHURE 4 BOLSILLOS',
                'value' => 'PORTA BROCHURE 4 BOLSILLOS',
                'description' => 'Otros Material'
            ],

            [
                'name' => 'PORTA BROCHURE PARA TARJETAS 4 BOLSILLOS',
                'value' => 'PORTA BROCHURE PARA TARJETAS 4 BOLSILLOS',
                'description' => 'Otros Material'
            ],

            [
                'name' => 'PORTA BROCHURE 1 BOLSILLO',
                'value' => 'PORTA BROCHURE 1 BOLSILLO',
                'description' => 'Otros Material'
            ],

            [
                'name' => 'ABANICOS 6.5" X 5.5" DOBLE CARA',
                'value' => 'ABANICOS 6.5" X 5.5" DOBLE CARA',
                'description' => 'Otros Material'
            ],

            [
                'name' => 'ABANICOS 6.5" X 5.5" UNA CARA',
                'value' => 'ABANICOS 6.5" X 5.5" UNA CARA',
                'description' => 'Otros Material'
            ],

            [
                'name' => 'BACKING  ALQUILER 90"X90" POR DIA',
                'value' => 'BACKING  ALQUILER 90"X90" POR DIA',
                'description' => 'Otros Material'
            ],

            [
                'name' => 'BACKING  ALQUILER + IMPRESIÒN 90"X90"',
                'value' => 'BACKING  ALQUILER + IMPRESIÒN 90"X90"',
                'description' => 'Otros Material'
            ],

            [
                'name' => 'MARCO DE PHOTOBOOTH FOAMBOARD 3`X2`',
                'value' => 'MARCO DE PHOTOBOOTH FOAMBOARD 3`X2`',
                'description' => 'Otros Material'
            ],

            [
                'name' => 'MARCO DE PHOTOBOOTH FOAMBOARD 4`X4`',
                'value' => 'MARCO DE PHOTOBOOTH FOAMBOARD 4`X4`',
                'description' => 'Otros Material'
            ],

            [
                'name' => 'PLACA DE RECONOCIMIENTO impresas en latex , fondo acrilico de 4 mm  TAMAÑO 8" X10"',
                'value' => 'PLACA DE RECONOCIMIENTO impresas en latex , fondo acrilico de 4 mm  TAMAÑO 8" X10"',
                'description' => 'Otros Material'
            ],

            [
                'name' => 'PLACA DE RECONOCIMIENTO EN MADERA CON LAMINA SUBLIMADA 7" X 5"',
                'value' => 'PLACA DE RECONOCIMIENTO EN MADERA CON LAMINA SUBLIMADA 7" X 5"',
                'description' => 'Otros Material'
            ],
        ]);


        DB::table('options')->insert([
            [
                'name' => 'BOTONES',
                'value' => '1.00',
                'description' => 'Otros Material Precio'
            ],

            [
                'name' => 'GAFETES  3"X1" O 3"X.75',
                'value' => '5.00',
                'description' => 'Otros Material Precio'
            ],

            [
                'name' => 'HABLADOR 8.5" X 11"',
                'value' => '13.00',
                'description' => 'Otros Material Precio'
            ],

            [
                'name' => 'PORTA BROCHURE 4 BOLSILLOS',
                'value' => '20.00',
                'description' => 'Otros Material Precio'
            ],

            [
                'name' => 'PORTA BROCHURE PARA TARJETAS 4 BOLSILLOS',
                'value' => '8.00',
                'description' => 'Otros Material Precio'
            ],

            [
                'name' => 'PORTA BROCHURE 1 BOLSILLO',
                'value' => '10.00',
                'description' => 'Otros Material Precio'
            ],

            [
                'name' => 'ABANICOS 6.5" X 5.5" DOBLE CARA',
                'value' => '1.75',
                'description' => 'Otros Material Precio'
            ],

            [
                'name' => 'ABANICOS 6.5" X 5.5" UNA CARA',
                'value' => '1.25',
                'description' => 'Otros Material Precio'
            ],

            [
                'name' => 'BACKING  ALQUILER 90"X90" POR DIA',
                'value' => '50.00',
                'description' => 'Otros Material Precio'
            ],

            [
                'name' => 'BACKING  ALQUILER + IMPRESIÒN 90"X90"',
                'value' => '160.00',
                'description' => 'Otros Material Precio'
            ],

            [
                'name' => 'MARCO DE PHOTOBOOTH FOAMBOARD 3`X2`',
                'value' => '27.00',
                'description' => 'Otros Material Precio'
            ],

            [
                'name' => 'MARCO DE PHOTOBOOTH FOAMBOARD 4`X4`',
                'value' => '56.00',
                'description' => 'Otros Material Precio'
            ],

            [
                'name' => 'PLACA DE RECONOCIMIENTO impresas en latex , fondo acrilico de 4 mm  TAMAÑO 8" X10"',
                'value' => '25.00',
                'description' => 'Otros Material Precio'
            ],

            [
                'name' => 'PLACA DE RECONOCIMIENTO EN MADERA CON LAMINA SUBLIMADA 7" X 5"',
                'value' => '35.00',
                'description' => 'Otros Material Precio'
            ],
        ]);


        DB::table('options')->insert([
            [
                'name' => 'BOTONES',
                'value' => '1.00',
                'description' => 'Otros Material Precio 12 unidades'
            ],

            [
                'name' => 'GAFETES  3"X1" O 3"X.75',
                'value' => '3.50',
                'description' => 'Otros Material Precio 12 unidades'
            ],

            [
                'name' => 'HABLADOR 8.5" X 11"',
                'value' => '13.00',
                'description' => 'Otros Material Precio 12 unidades'
            ],

            [
                'name' => 'PORTA BROCHURE 4 BOLSILLOS',
                'value' => '20.00',
                'description' => 'Otros Material Precio 12 unidades'
            ],

            [
                'name' => 'PORTA BROCHURE PARA TARJETAS 4 BOLSILLOS',
                'value' => '8.00',
                'description' => 'Otros Material Precio 12 unidades'
            ],

            [
                'name' => 'PORTA BROCHURE 1 BOLSILLO',
                'value' => '10.00',
                'description' => 'Otros Material Precio 12 unidades'
            ],

            [
                'name' => 'ABANICOS 6.5" X 5.5" DOBLE CARA',
                'value' => '1.75',
                'description' => 'Otros Material Precio 12 unidades'
            ],

            [
                'name' => 'ABANICOS 6.5" X 5.5" UNA CARA',
                'value' => '1.25',
                'description' => 'Otros Material Precio 12 unidades'
            ],

            [
                'name' => 'BACKING  ALQUILER 90"X90" POR DIA',
                'value' => '50.00',
                'description' => 'Otros Material Precio 12 unidades'
            ],

            [
                'name' => 'BACKING  ALQUILER + IMPRESIÒN 90"X90"',
                'value' => '160.00',
                'description' => 'Otros Material Precio 12 unidades'
            ],

            [
                'name' => 'MARCO DE PHOTOBOOTH FOAMBOARD 3`X2`',
                'value' => '27.00',
                'description' => 'Otros Material Precio 12 unidades'
            ],

            [
                'name' => 'MARCO DE PHOTOBOOTH FOAMBOARD 4`X4`',
                'value' => '56.00',
                'description' => 'Otros Material Precio 12 unidades'
            ],

            [
                'name' => 'PLACA DE RECONOCIMIENTO impresas en latex , fondo acrilico de 4 mm  TAMAÑO 8" X10"',
                'value' => '16.00',
                'description' => 'Otros Material Precio 12 unidades'
            ],

            [
                'name' => 'PLACA DE RECONOCIMIENTO EN MADERA CON LAMINA SUBLIMADA 7" X 5"',
                'value' => '30.00',
                'description' => 'Otros Material Precio 12 unidades'
            ],
        ]);

        /*
        * Toldas and Carpas
        */
        DB::table('item_options')->insert([
            [
                'item' => 3,
                'option' => 8
            ],

            [
                'item' => 3,
                'option' => 9
            ],

            [
                'item' => 3,
                'option' => 10
            ],

            [
                'item' => 3,
                'option' => 11
            ],

            [
                'item' => 3,
                'option' => 12
            ],

            [
                'item' => 3,
                'option' => 13
            ],

            [
                'item' => 3,
                'option' => 14
            ],

            [
                'item' => 3,
                'option' => 15
            ],

            [
                'item' => 3,
                'option' => 16
            ],

            [
                'item' => 3,
                'option' => 17
            ],

            [
                'item' => 3,
                'option' => 18
            ],

            [
                'item' => 3,
                'option' => 19
            ],

            [
                'item' => 3,
                'option' => 20
            ],

            [
                'item' => 3,
                'option' => 21
            ],

            [
                'item' => 3,
                'option' => 22
            ],

            [
                'item' => 3,
                'option' => 23
            ],

            [
                'item' => 3,
                'option' => 24
            ],

            [
                'item' => 3,
                'option' => 25
            ],

            [
                'item' => 3,
                'option' => 26
            ],

            [
                'item' => 3,
                'option' => 27
            ],

            [
                'item' => 3,
                'option' => 28
            ],

            [
                'item' => 3,
                'option' => 29
            ],

            [
                'item' => 3,
                'option' => 30
            ],

            [
                'item' => 3,
                'option' => 31
            ],

            [
                'item' => 3,
                'option' => 32
            ],

            [
                'item' => 3,
                'option' => 33
            ],

            [
                'item' => 3,
                'option' => 34
            ],


            [
                'item' => 3,
                'option' => 35
            ],

            [
                'item' => 3,
                'option' => 36
            ],

            [
                'item' => 3,
                'option' => 37
            ],

            [
                'item' => 3,
                'option' => 38
            ],

            [
                'item' => 3,
                'option' => 39
            ],

            [
                'item' => 3,
                'option' => 40
            ],

            [
                'item' => 3,
                'option' => 41
            ],

            [
                'item' => 3,
                'option' => 42
            ],

            [
                'item' => 3,
                'option' => 43
            ],

            [
                'item' => 3,
                'option' => 44
            ],

            [
                'item' => 3,
                'option' => 45
            ],

            [
                'item' => 3,
                'option' => 46
            ],

            [
                'item' => 3,
                'option' => 47
            ],

        ]);

        /*
         * Toldas Sector < 15 Pies
         */
        DB::table('item_options')->insert([
            [
                'item' => 4,
                'option' => 48
            ],
            [
                'item' => 4,
                'option' => 49
            ],

            [
                'item' => 4,
                'option' => 50
            ],

            [
                'item' => 4,
                'option' => 51
            ],
            [
                'item' => 4,
                'option' => 52
            ],


        ]);

        /*
         * Toldas Sector > 15 Pies
         */
        DB::table('item_options')->insert([
            [
                'item' => 5,
                'option' => 53
            ],
            [
                'item' => 5,
                'option' => 54
            ],

            [
                'item' => 5,
                'option' => 55
            ],

            [
                'item' => 5,
                'option' => 56
            ],
            [
                'item' => 5,
                'option' => 57
            ],


        ]);

        /*
         * Material Corte
         */
        DB::table('item_options')->insert([
            [
                'item' => 6,
                'option' => 58
            ],
            [
                'item' => 6,
                'option' => 59
            ],

            [
                'item' => 6,
                'option' => 60
            ],

            [
                'item' => 6,
                'option' => 61
            ],
            [
                'item' => 6,
                'option' => 62
            ],

            [
                'item' => 6,
                'option' => 63
            ],

            [
                'item' => 6,
                'option' => 64
            ],

            [
                'item' => 6,
                'option' => 65
            ],


        ]);


        /*
         * Iluminacion
         */
        DB::table('item_options')->insert([
            [
                'item' => 7,
                'option' => 66
            ],

            [
                'item' => 7,
                'option' => 67
            ],

            [
                'item' => 8,
                'option' => 68
            ],

            [
                'item' => 8,
                'option' => 69
            ],

            [
                'item' => 9,
                'option' => 70
            ],

            [
                'item' => 9,
                'option' => 71
            ],

            [
                'item' => 10,
                'option' => 72
            ],

            [
                'item' => 10,
                'option' => 73
            ],

            [
                'item' => 10,
                'option' => 74
            ],

            [
                'item' => 10,
                'option' => 75
            ],

            [
                'item' => 10,
                'option' => 76
            ],

            [
                'item' => 10,
                'option' => 77
            ],

            [
                'item' => 10,
                'option' => 78
            ],

            [
                'item' => 10,
                'option' => 79
            ],

            [
                'item' => 10,
                'option' => 80
            ],

            [
                'item' => 10,
                'option' => 81
            ],

            [
                'item' => 10,
                'option' => 82
            ],

            [
                'item' => 11,
                'option' => 83
            ],

            [
                'item' => 11,
                'option' => 84
            ],

            [
                'item' => 11,
                'option' => 85
            ],


            [
                'item' => 11,
                'option' => 86
            ],

            [
                'item' => 11,
                'option' => 87
            ],

            [
                'item' => 12,
                'option' => 88
            ],

            [
                'item' => 12,
                'option' => 89
            ],

            [
                'item' => 12,
                'option' => 90
            ],

            [
                'item' => 12,
                'option' => 91
            ],

            [
                'item' => 12,
                'option' => 92
            ],

            [
                'item' => 13,
                'option' => 93
            ],

            [
                'item' => 13,
                'option' => 94
            ],

            [
                'item' => 13,
                'option' => 95
            ],

            [
                'item' => 13,
                'option' => 96
            ],

            [
                'item' => 13,
                'option' => 97
            ],
        ]);

        DB::table('item_options')->insert([
            [
                'item' => 14,
                'option' => 98,
            ],

            [
                'item' => 14,
                'option' => 99,
            ],

            [
                'item' => 14,
                'option' => 100,
            ],

            [
                'item' => 14,
                'option' => 101,
            ],

            [
                'item' => 14,
                'option' => 102,
            ],

            [
                'item' => 15,
                'option' => 103,
            ],

            [
                'item' => 15,
                'option' => 104,
            ],

            [
                'item' => 16,
                'option' => 105,
            ],

            [
                'item' => 16,
                'option' => 106,
            ],

            [
                'item' => 16,
                'option' => 107,
            ],

            [
                'item' => 16,
                'option' => 108,
            ],

            [
                'item' => 17,
                'option' => 109,
            ],

            [
                'item' => 17,
                'option' => 110,
            ],

            [
                'item' => 18,
                'option' => 111,
            ],

            [
                'item' => 18,
                'option' => 112,
            ],

        ]);


        DB::table('item_options')->insert([
            [
                'item' => 19,
                'option' => 113,
            ],

            [
                'item' => 19,
                'option' => 114,
            ],

            [
                'item' => 19,
                'option' => 115,
            ],

            [
                'item' => 19,
                'option' => 116,
            ],

            [
                'item' => 19,
                'option' => 117,
            ],

            [
                'item' => 19,
                'option' => 118,
            ],

            [
                'item' => 20,
                'option' => 119,
            ],

            [
                'item' => 20,
                'option' => 120,
            ],

            [
                'item' => 20,
                'option' => 121,
            ],

            [
                'item' => 20,
                'option' => 122,
            ],


            [
                'item' => 20,
                'option' => 123,
            ],

            [
                'item' => 20,
                'option' => 124,
            ],

            [
                'item' => 21,
                'option' => 125,
            ],

            [
                'item' => 21,
                'option' => 126,
            ],

            [
                'item' => 21,
                'option' => 127,
            ],

            [
                'item' => 21,
                'option' => 128,
            ],

            [
                'item' => 21,
                'option' => 129,
            ],

            [
                'item' => 21,
                'option' => 130,
            ],

            [
                'item' => 22,
                'option' => 131,
            ],

            [
                'item' => 22,
                'option' => 132,
            ],

            [
                'item' => 22,
                'option' => 133,
            ],

            [
                'item' => 22,
                'option' => 134,
            ],

            [
                'item' => 22,
                'option' => 135,
            ],


            [
                'item' => 22,
                'option' => 136,
            ],

            [
                'item' => 23,
                'option' => 137,
            ],

            [
                'item' => 23,
                'option' => 138,
            ],

            [
                'item' => 23,
                'option' => 139,
            ],

            [
                'item' => 23,
                'option' => 140,
            ],

            [
                'item' => 23,
                'option' => 141,
            ],

            [
                'item' => 23,
                'option' => 142,
            ],

            [
                'item' => 24,
                'option' => 143,
            ],

            [
                'item' => 24,
                'option' => 144,
            ],


            [
                'item' => 24,
                'option' => 145,
            ],


            [
                'item' => 24,
                'option' => 146,
            ],


            [
                'item' => 24,
                'option' => 147,
            ],

            [
                'item' => 24,
                'option' => 148,
            ],

            [
                'item' => 25,
                'option' => 149,
            ],

            [
                'item' => 25,
                'option' => 150,
            ],

            [
                'item' => 25,
                'option' => 151,
            ],

            [
                'item' => 25,
                'option' => 152,
            ],

            [
                'item' => 25,
                'option' => 153,
            ],

            [
                'item' => 25,
                'option' => 154,
            ],

            [
                'item' => 26,
                'option' => 155,
            ],

            [
                'item' => 26,
                'option' => 156,
            ],

            [
                'item' => 26,
                'option' => 157,
            ],

            [
                'item' => 26,
                'option' => 158,
            ],

            [
                'item' => 26,
                'option' => 159,
            ],

            [
                'item' => 26,
                'option' => 160,
            ],

            [
                'item' => 27,
                'option' => 161,
            ],

            [
                'item' => 27,
                'option' => 162,
            ],

            [
                'item' => 27,
                'option' => 163,
            ],

            [
                'item' => 28,
                'option' => 164,
            ],

            [
                'item' => 28,
                'option' => 165,
            ],

            [
                'item' => 29,
                'option' => 166,
            ],

            [
                'item' => 29,
                'option' => 167,
            ],

            [
                'item' => 30,
                'option' => 168,
            ],

            [
                'item' => 30,
                'option' => 169,
            ],
        ]);

        DB::table('item_options')->insert([
                [
                    'item' => 31,
                    'option' => 170,
                ],

                [
                    'item' => 31,
                    'option' => 171,
                ],

                [
                    'item' => 31,
                    'option' => 172,
                ],

                [
                    'item' => 31,
                    'option' => 173,
                ],

                [
                    'item' => 31,
                    'option' => 174,
                ],

                [
                    'item' => 31,
                    'option' => 175,
                ],

                [
                    'item' => 31,
                    'option' => 176,
                ],

                [
                    'item' => 31,
                    'option' => 177,
                ],

                [
                    'item' => 31,
                    'option' => 178,
                ],

                [
                    'item' => 31,
                    'option' => 179,
                ],

                [
                    'item' => 31,
                    'option' => 180,
                ],

                [
                    'item' => 31,
                    'option' => 181,
                ],

                [
                    'item' => 31,
                    'option' => 182,
                ],

                [
                    'item' => 31,
                    'option' => 183,
                ],

                [
                    'item' => 32,
                    'option' => 184,
                ],

                [
                    'item' => 32,
                    'option' => 185,
                ],

                [
                    'item' => 32,
                    'option' => 186,
                ],

                [
                    'item' => 32,
                    'option' => 187,
                ],

                [
                    'item' => 33,
                    'option' => 188,
                ],

                [
                    'item' => 33,
                    'option' => 189,
                ],

                [
                    'item' => 33,
                    'option' => 190,
                ],

                [
                    'item' => 33,
                    'option' => 191,
                ],

                [
                    'item' => 33,
                    'option' => 192,
                ],

                [
                    'item' => 33,
                    'option' => 193,
                ],

                [
                    'item' => 33,
                    'option' => 194,
                ],

                [
                    'item' => 33,
                    'option' => 195,
                ],

                [
                    'item' => 33,
                    'option' => 196,
                ],

                [
                    'item' => 33,
                    'option' => 197,
                ],

                [
                    'item' => 33,
                    'option' => 198,
                ],

                [
                    'item' => 33,
                    'option' => 199,
                ],

                [
                    'item' => 33,
                    'option' => 200,
                ],

                [
                    'item' => 33,
                    'option' => 201,
                ],


                /*
                 *
                 */

                [
                    'item' => 34,
                    'option' => 202,
                ],

                [
                    'item' => 34,
                    'option' => 203,
                ],

                [
                    'item' => 34,
                    'option' => 204,
                ],

                [
                    'item' => 34,
                    'option' => 205,
                ],

                [
                    'item' => 34,
                    'option' => 206,
                ],

                [
                    'item' => 34,
                    'option' => 207,
                ],

                [
                    'item' => 34,
                    'option' => 208,
                ],

                [
                    'item' => 34,
                    'option' => 209,
                ],

                [
                    'item' => 34,
                    'option' => 210,
                ],

                [
                    'item' => 34,
                    'option' => 211,
                ],

                [
                    'item' => 34,
                    'option' => 212,
                ],

                [
                    'item' => 34,
                    'option' => 213,
                ],

                [
                    'item' => 34,
                    'option' => 214,
                ],

                [
                    'item' => 34,
                    'option' => 215,
                ],

                [
                    'item' => 35,
                    'option' => 216,
                ],


                [
                    'item' => 35,
                    'option' => 217,
                ],

                [
                    'item' => 35,
                    'option' => 218,
                ],

                [
                    'item' => 35,
                    'option' => 219,
                ],

                [
                    'item' => 35,
                    'option' => 220,
                ],

                [
                    'item' => 35,
                    'option' => 221,
                ],

                [
                    'item' => 35,
                    'option' => 222,
                ],

                [
                    'item' => 35,
                    'option' => 223,
                ],

                [
                    'item' => 35,
                    'option' => 224,
                ],

                [
                    'item' => 35,
                    'option' => 225,
                ],

                [
                    'item' => 35,
                    'option' => 226,
                ],

                [
                    'item' => 35,
                    'option' => 227,
                ],

                [
                    'item' => 35,
                    'option' => 228,
                ],

                [
                    'item' => 35,
                    'option' => 229,
                ],

                [
                    'item' => 36,
                    'option' => 230,
                ],

                [
                    'item' => 36,
                    'option' => 231,
                ],

                [
                    'item' => 36,
                    'option' => 232,
                ],

                [
                    'item' => 36,
                    'option' => 233,
                ],

                [
                    'item' => 36,
                    'option' => 234,
                ],

                [
                    'item' => 36,
                    'option' => 235,
                ],

                [
                    'item' => 36,
                    'option' => 236,
                ],

                [
                    'item' => 36,
                    'option' => 237,
                ],

                [
                    'item' => 36,
                    'option' => 238,
                ],

                [
                    'item' => 36,
                    'option' => 239,
                ],

                [
                    'item' => 36,
                    'option' => 240,
                ],

                [
                    'item' => 36,
                    'option' => 241,
                ],

                [
                    'item' => 36,
                    'option' => 242,
                ],

                [
                    'item' => 36,
                    'option' => 243,
                ],

                [
                    'item' => 37,
                    'option' => 244,
                ],

                [
                    'item' => 37,
                    'option' => 245,
                ],

                [
                    'item' => 37,
                    'option' => 246,
                ],

                [
                    'item' => 37,
                    'option' => 247,
                ],


            ]
        );

        /*
         *  Facias
         */
        DB::table('item_options')->insert([
            [
                'item' => 38,
                'option' => 248,
            ],

            [
                'item' => 38,
                'option' => 249,
            ],

            [
                'item' => 39,
                'option' => 250,
            ],

            [
                'item' => 39,
                'option' => 251,
            ],

            [
                'item' => 39,
                'option' => 252,
            ],

            [
                'item' => 39,
                'option' => 253,
            ],

            [
                'item' => 39,
                'option' => 254,
            ],

            [
                'item' => 40,
                'option' => 255,
            ],

            [
                'item' => 40,
                'option' => 256,
            ],

            [
                'item' => 40,
                'option' => 257,
            ],

            [
                'item' => 40,
                'option' => 258,
            ],

            [
                'item' => 40,
                'option' => 259,
            ],

            [
                'item' => 41,
                'option' => 260,
            ],

            [
                'item' => 41,
                'option' => 261,
            ],
            [
                'item' => 41,
                'option' => 262,
            ],

            [
                'item' => 41,
                'option' => 263,
            ],

            [
                'item' => 41,
                'option' => 264,
            ],


            [
                'item' => 42,
                'option' => 265,
            ],

            [
                'item' => 42,
                'option' => 266,
            ],

            [
                'item' => 42,
                'option' => 267,
            ],

            [
                'item' => 42,
                'option' => 268,
            ],

            [
                'item' => 43,
                'option' => 269,
            ],


            [
                'item' => 43,
                'option' => 270,
            ],


            [
                'item' => 43,
                'option' => 271,
            ],


            [
                'item' => 43,
                'option' => 272,
            ],


            [
                'item' => 44,
                'option' => 273,
            ],

            [
                'item' => 44,
                'option' => 274,
            ],

            [
                'item' => 44,
                'option' => 275,
            ],

            [
                'item' => 44,
                'option' => 276,
            ],

            [
                'item' => 45,
                'option' => 277,
            ],

            [
                'item' => 45,
                'option' => 278,
            ],

            [
                'item' => 45,
                'option' => 279,
            ],
            [
                'item' => 45,
                'option' => 280,
            ],


            /*
             *
             */

            [
                'item' => 46,
                'option' => 281,
            ],

            [
                'item' => 46,
                'option' => 282,
            ],

            [
                'item' => 46,
                'option' => 283,
            ],
            [
                'item' => 46,
                'option' => 284,
            ],


        ]);


        DB::table('item_options')->insert([
            [
                'item' => 47,
                'option' => 285,
            ],

            [
                'item' => 47,
                'option' => 286,
            ],


            [
                'item' => 47,
                'option' => 287,
            ],

            [
                'item' => 47,
                'option' => 288,
            ],

            [
                'item' => 47,
                'option' => 289,
            ],

            [
                'item' => 47,
                'option' => 290,
            ],

            [
                'item' => 47,
                'option' => 291,
            ],


            [
                'item' => 47,
                'option' => 292,
            ],

            [
                'item' => 47,
                'option' => 293,
            ],

            [
                'item' => 47,
                'option' => 294,
            ],

            [
                'item' => 47,
                'option' => 295,
            ],

            [
                'item' => 47,
                'option' => 296,
            ],

            [
                'item' => 47,
                'option' => 297,
            ],

            [
                'item' => 47,
                'option' => 298,
            ],

            [
                'item' => 47,
                'option' => 299,
            ],

            [
                'item' => 47,
                'option' => 300,
            ],

            [
                'item' => 47,
                'option' => 301,
            ],

            [
                'item' => 47,
                'option' => 302,
            ],

            [
                'item' => 47,
                'option' => 303,
            ],


            [
                'item' => 47,
                'option' => 304,
            ],

            [
                'item' => 47,
                'option' => 305,
            ],

            [
                'item' => 47,
                'option' => 306,
            ],

            [
                'item' => 47,
                'option' => 307,
            ],

            [
                'item' => 47,
                'option' => 308,
            ],

            [
                'item' => 47,
                'option' => 309,
            ],

            [
                'item' => 47,
                'option' => 310,
            ],

            [
                'item' => 47,
                'option' => 311,
            ],

            [
                'item' => 47,
                'option' => 312,
            ],

            [
                'item' => 47,
                'option' => 313,
            ],

            [
                'item' => 47,
                'option' => 314,
            ],

            [
                'item' => 47,
                'option' => 315,
            ],

            [
                'item' => 47,
                'option' => 316,
            ],

            [
                'item' => 47,
                'option' => 317,
            ],

            [
                'item' => 47,
                'option' => 318,
            ],

            [
                'item' => 47,
                'option' => 319,
            ],

            [
                'item' => 47,
                'option' => 320,
            ],

            [
                'item' => 47,
                'option' => 321,
            ],

            [
                'item' => 47,
                'option' => 322,
            ],

            [
                'item' => 47,
                'option' => 323,
            ],

            [
                'item' => 47,
                'option' => 324,
            ],

            [
                'item' => 47,
                'option' => 325,
            ],


            [
                'item' => 47,
                'option' => 326,
            ],


            [
                'item' => 47,
                'option' => 327,
            ],

            [
                'item' => 47,
                'option' => 328,
            ],

            [
                'item' => 47,
                'option' => 329,
            ],

            [
                'item' => 47,
                'option' => 330,
            ],

            [
                'item' => 47,
                'option' => 331,
            ],

            [
                'item' => 47,
                'option' => 332,
            ],

            [
                'item' => 48,
                'option' => 333,
            ],

            [
                'item' => 48,
                'option' => 334,
            ],


            [
                'item' => 48,
                'option' => 335,
            ],


            [
                'item' => 48,
                'option' => 336,
            ],


            [
                'item' => 48,
                'option' => 337,
            ],


            [
                'item' => 48,
                'option' => 338,
            ],

            [
                'item' => 49,
                'option' => 339,
            ],

            [
                'item' => 49,
                'option' => 340,
            ],

            [
                'item' => 49,
                'option' => 341,
            ],

            [
                'item' => 49,
                'option' => 342,
            ],


            [
                'item' => 49,
                'option' => 343,
            ],


            [
                'item' => 49,
                'option' => 344,
            ],


            [
                'item' => 49,
                'option' => 345,
            ],

            [
                'item' => 50,
                'option' => 346,
            ],


            [
                'item' => 50,
                'option' => 347,
            ],

            [
                'item' => 50,
                'option' => 348,
            ],

            [
                'item' => 50,
                'option' => 349,
            ],

            [
                'item' => 50,
                'option' => 350,
            ],

            [
                'item' => 50,
                'option' => 351,
            ],

            [
                'item' => 50,
                'option' => 352,
            ],


            [
                'item' => 50,
                'option' => 353,
            ],

            [
                'item' => 50,
                'option' => 354,
            ],

            [
                'item' => 50,
                'option' => 355,
            ],

            [
                'item' => 50,
                'option' => 356,
            ],

            [
                'item' => 50,
                'option' => 357,
            ],

            [
                'item' => 50,
                'option' => 358,
            ],

            [
                'item' => 50,
                'option' => 359,
            ],

            [
                'item' => 50,
                'option' => 360,
            ],

            [
                'item' => 50,
                'option' => 361,
            ],

            [
                'item' => 50,
                'option' => 362,
            ],


            [
                'item' => 50,
                'option' => 363,
            ],

            [
                'item' => 50,
                'option' => 364,
            ],

            [
                'item' => 50,
                'option' => 365,
            ],

            [
                'item' => 50,
                'option' => 366,
            ],

            [
                'item' => 51,
                'option' => 367,
            ],

            [
                'item' => 51,
                'option' => 368,
            ],

            [
                'item' => 51,
                'option' => 369,
            ],

            [
                'item' => 51,
                'option' => 370,
            ],

            [
                'item' => 51,
                'option' => 371,
            ],

            [
                'item' => 51,
                'option' => 372,
            ],
            [
                'item' => 51,
                'option' => 373,
            ],
            [
                'item' => 51,
                'option' => 374,
            ],
            [
                'item' => 51,
                'option' => 375,
            ],

            [
                'item' => 51,
                'option' => 376,
            ],

            [
                'item' => 51,
                'option' => 377,
            ],

            [
                'item' => 51,
                'option' => 378,
            ],

            [
                'item' => 51,
                'option' => 379,
            ],

            [
                'item' => 51,
                'option' => 380,
            ],

            [
                'item' => 51,
                'option' => 381,
            ],

            [
                'item' => 51,
                'option' => 381,
            ],


            [
                'item' => 51,
                'option' => 382,
            ],


            [
                'item' => 51,
                'option' => 383,
            ],

            [
                'item' => 51,
                'option' => 384,
            ],

            [
                'item' => 51,
                'option' => 385,
            ],

            [
                'item' => 51,
                'option' => 386,
            ],

            [
                'item' => 51,
                'option' => 387,
            ],

            [
                'item' => 52,
                'option' => 388,
            ],

            [
                'item' => 52,
                'option' => 389,
            ],

            [
                'item' => 52,
                'option' => 390,
            ],

            [
                'item' => 52,
                'option' => 391,
            ],

            [
                'item' => 52,
                'option' => 392,
            ],

            [
                'item' => 52,
                'option' => 393,
            ],

            [
                'item' => 52,
                'option' => 394,
            ],

            [
                'item' => 52,
                'option' => 395,
            ],

            [
                'item' => 52,
                'option' => 396,
            ],

            [
                'item' => 52,
                'option' => 397,
            ],
            [
                'item' => 52,
                'option' => 398,
            ],

            [
                'item' => 52,
                'option' => 399,
            ],

            [
                'item' => 52,
                'option' => 400,
            ],

            [
                'item' => 52,
                'option' => 401,
            ],

            [
                'item' => 52,
                'option' => 402,
            ],

            [
                'item' => 52,
                'option' => 403,
            ],

            [
                'item' => 52,
                'option' => 404,
            ],

            [
                'item' => 52,
                'option' => 405,
            ],


            [
                'item' => 52,
                'option' => 406,
            ],

            [
                'item' => 52,
                'option' => 407,
            ],

            [
                'item' => 52,
                'option' => 408,
            ],

            [
                'item' => 53,
                'option' => 409,
            ],


            [
                'item' => 53,
                'option' => 410,
            ],

            [
                'item' => 53,
                'option' => 411,
            ],

            [
                'item' => 53,
                'option' => 412,
            ],

            [
                'item' => 53,
                'option' => 413,
            ],

            [
                'item' => 53,
                'option' => 414,
            ],

            [
                'item' => 53,
                'option' => 415,
            ],

            [
                'item' => 53,
                'option' => 416,
            ],


            [
                'item' => 53,
                'option' => 417,
            ],


            [
                'item' => 53,
                'option' => 418,
            ],


            [
                'item' => 53,
                'option' => 419,
            ],


            [
                'item' => 53,
                'option' => 420,
            ],


            [
                'item' => 53,
                'option' => 421,
            ],


            [
                'item' => 53,
                'option' => 422,
            ],


            [
                'item' => 54,
                'option' => 423,
            ],


            [
                'item' => 54,
                'option' => 424,
            ],


            [
                'item' => 54,
                'option' => 425,
            ],


            [
                'item' => 54,
                'option' => 426,
            ],


            [
                'item' => 54,
                'option' => 427,
            ],

            [
                'item' => 54,
                'option' => 428,
            ],

            [
                'item' => 54,
                'option' => 429,
            ],


            [
                'item' => 54,
                'option' => 430,
            ],

            [
                'item' => 54,
                'option' => 431,
            ],


            [
                'item' => 54,
                'option' => 432,
            ],


            [
                'item' => 54,
                'option' => 433,
            ],

            [
                'item' => 54,
                'option' => 434,
            ],

            [
                'item' => 54,
                'option' => 435,
            ],

            [
                'item' => 54,
                'option' => 436,
            ],

            [
                'item' => 55,
                'option' => 437,
            ],

            [
                'item' => 55,
                'option' => 438,
            ],

            [
                'item' => 55,
                'option' => 439,
            ],

            [
                'item' => 55,
                'option' => 440,
            ],

            [
                'item' => 55,
                'option' => 441,
            ],

            [
                'item' => 55,
                'option' => 442,
            ],
            [
                'item' => 55,
                'option' => 443,
            ],

            [
                'item' => 55,
                'option' => 444,
            ],

            [
                'item' => 55,
                'option' => 445,
            ],

            [
                'item' => 55,
                'option' => 446,
            ],
            [
                'item' => 55,
                'option' => 447,
            ],

            [
                'item' => 55,
                'option' => 448,
            ],

            [
                'item' => 55,
                'option' => 449,
            ],

            [
                'item' => 55,
                'option' => 450,
            ],





        ]);


    }
}
