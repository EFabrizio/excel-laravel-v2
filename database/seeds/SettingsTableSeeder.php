<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            [
                'name' => 'Sitio Web',
                'value' => 'Calvo',
                'description' => 'Nombre oficial del sitio web'
            ],
            [
                'name' => 'Logo',
                'value' => 'Calvo.png',
                'description' => 'Logo del sitio'
            ],
        ]);
    }
}
