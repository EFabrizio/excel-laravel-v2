@extends('layouts.dashboard')

@section('content')

    <section class="statistic">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    @if(Auth::user()->rank == 0)
                        <h1 class="text-uppercase">Bienvenido al sistema Administrador</h1>
                    @elseif(Auth::user()->rank == 1)
                        <h1 class="text-uppercase">Bienvenido al sistema Usuario</h1>
                    @endif
                </div>
            </div>
        </div>
    </section>


@stop
