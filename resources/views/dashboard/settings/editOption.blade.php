
@extends('layouts.dashboard')

@section('content')



    <div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title" id="basic-layout-colored-form-control">Editar</h4>
            </div>

            <div class="card-content collapse show">


                <div class="card-body">

                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <form method="POST" action="{{ route('optionEdit') }}">
                        @csrf
                        <input type="hidden" name="option_id" value="{{ $option->id }}">
                        <div class="form-body">
                            <div class="form-group">
                                <label for="userinput6">Nombre</label>
                                <input class="form-control" type="text" name="name" value="{{ $option->name }}">
                            </div>


                            <div class="form-group">
                                <label for="userinput6">Valor</label>
                                <input class="form-control" type="text" value="{{ $option->value }}" name="value">
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="userinput6">Descripcion</label>
                                <input class="form-control" type="text" name="description" value="{{ $option->description }}">
                            </div>

                            <div class="form-actions left">
                                <button type="submit" class="btn btn-primary">Editar</button>
                                <a href="{{ route('varSettings') }}" class="btn btn-danger"> Regresar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div>
@stop
