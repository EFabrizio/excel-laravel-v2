@extends('layouts.dashboard')

@section('content')


    <section class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row my-3">
                <h3 class="title-3">Configuración de VAR</h3>
            </div>

            <div class="row">
                <div class="table-responsive table--no-card m-b-30">
                    <table class="table table-borderless table-striped table-earning">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Valor</th>
                            <th>Descripción </th>
                            <th>Opciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($itemOptions as $item)
                            <tr>
                                <td>{{ $item->option->name }}</td>
                                <td>{{ $item->option->value }}</td>
                                <td>{{ $item->option->description }}</td>
                                <td>
                                    <a href="{{ route('showOptionEdit', ['id' => $item->id ]) }}" class="btn btn-primary">Editar</a>
                                    <a href="{{ route('showOptionRemove', ['id' => $item->id ]) }}" class="btn btn-danger">Eliminar</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>


                <a href="{{ route('varSettings') }}" class="btn btn-danger">Regresar</a>
            </div>
        </div>
    </section>
@stop
