@extends('layouts.dashboard')

@section('content')


    <section class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row my-3">
                <h3 class="title-3">Configuración de VAR</h3>
            </div>

            <div class="row">
                <div class="table-responsive table--no-card m-b-30">
                    <table class="table table-borderless table-striped table-earning">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>No. Opciones</th>
                            <th>Opciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($items as $item)
                            <tr>
                                <td>{{ $item->description }}</td>
                                <td><span class="badge badge-success">{{ $item->options }}</span></td>
                                <td><a href="{{ route('itemOptions', ['id' => $item->id ]) }}" class="btn btn-primary">Visualizar</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@stop
