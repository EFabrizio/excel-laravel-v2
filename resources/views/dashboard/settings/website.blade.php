@extends('layouts.dashboard')

@section('content')


    <section class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row my-3">
                <h3 class="title-3">Configuración de Sitio Web</h3>
            </div>

            <div class="row">
                <div class="table-responsive table--no-card m-b-30">
                    <table class="table table-borderless table-striped table-earning">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Valor</th>
                                <th>Descripción</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($settings as $setting)
                            <tr>
                                <td>{{ $setting->name }}</td>
                                <td>{{ $setting->value }}</td>
                                <td>{{ $setting->description }}</td>
                                <td><a href="#" class="btn btn-primary">Editar</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@stop
