@extends('layouts.dashboard')

@section('content')

    <div id="app">
        <div class="container ">
            <div class="row justify-content-center">
                <div class="col-md-10">

                    <div class="card">
                        <div class="card-header">
                            <strong>Carpas</strong>
                        </div>
                        <div class="card-body card-block">
                            <form class="form-horizontal">
                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Tamaño en pies</label>
                                    </div>
                                    <div class="col col-sm-5">
                                        <input type="text" v-model="width" v-on:keyup="calculatePrice"
                                               placeholder="Largo"
                                               class="form-control">
                                    </div>
                                    <div class="col col-sm-5">
                                        <input type="text" v-model="height" v-on:keyup="calculatePrice"
                                               placeholder="Alto"
                                               class="form-control">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Material</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <select v-model="material_option" @change="calculatePrice" class="form-control">
                                            <option>Seleccionar Material</option>
                                            @foreach($itemOptions as $item)
                                                <option
                                                    value="{{ $item->option->value }}">{{ $item->option->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Precio sin
                                            instalación</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <input type="text" v-model="price_no_install"
                                               class="form-control"
                                               readonly>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-sm-12 text-danger">
                                        <span><b>El precio no incluye Rotulacion ni servicio de pintura.</b> </span>
                                    </div>
                                </div>


                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row justify-content-center">
                <div class="col-md-10">
                    <div class="card">
                        <div class="card-header">
                            <strong>Convertidor de Medidas a pies</strong>
                        </div>
                        <div class="card-body card-block">
                            <div class="row form-group">
                                <div class="col col-sm-2">
                                    <label for="input-small" class=" form-control-label">Numero a convertir</label>
                                </div>
                                <div class="col col-sm-10">
                                    <input type="text" v-on:keyup="calculateFt" v-model="number_convert"
                                           class="form-control">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-sm-2">
                                    <label for="input-small" class=" form-control-label">Unidad de medida</label>
                                </div>
                                <div class="col col-sm-10">
                                    <select v-model="unit_select" @change="calculateFt"
                                            class="form-control">
                                        <option>Seleccionar Unidad</option>
                                        <option value="Pulgadas">Pulgadas</option>
                                        <option value="Metros">Metros</option>
                                        <option value="Centimetros">Centimetros</option>
                                        <option value="Milimetros">Milimetros</option>


                                    </select>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-sm-2">
                                    <label for="input-small" class=" form-control-label">Resultado en pies</label>
                                </div>
                                <div class="col col-sm-10">
                                    <input type="text" v-model="ft_result"
                                           class="form-control" readonly>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>

    <script>
        var app = new Vue({
            el: '#app',
            data: {
                price_no_install: '',
                width: '',
                height: '',
                dataPrice: '',
                material_option: 'Seleccionar Material',
                number_convert: '',
                unit_select: 'Seleccionar Unidad',
                ft_result: '',
            },
            mounted() {
                axios
                    .get('/var/data/2')
                    .then(response => (this.dataPrice = response.data))

            },
            methods: {
                calculatePrice: function () {

                    if (this.material_option != 'Seleccionar Material') {
                        if (this.width < 10) {
                            this.price_no_install = 'El largo solo acepta valores del 10 al 20.'
                        } else if (this.width > 20) {
                            this.price_no_install = 'El largo solo acepta valores del 10 al 20.'
                        } else if (this.height > 15) {
                            this.price_no_install = 'El alto solo acepta valores del 10 al 15.'
                        } else if (this.height < 10) {
                            this.price_no_install = 'El alto solo acepta valores del 10 al 15.'
                        } else if (this.width >= 10 && this.width <= 15 && this.material_option == this.dataPrice[0].value) {
                            this.price_no_install = 'B/.' + this.width * this.height * this.dataPrice[2].value
                        } else
                            this.price_no_install = 'B/.' + this.width * this.height * this.material_option
                    }

                },
                calculateFt: function () {

                    if (this.unit_select === 'Pulgadas') {
                        this.ft_result = this.number_convert / 12
                    } else if (this.unit_select === 'Metros') {
                        this.ft_result = this.number_convert / 0.3048
                    } else if (this.unit_select === 'Centimetros') {
                        this.ft_result = this.number_convert / 30.48
                    } else if (this.unit_select === 'Milimetros') {
                        this.ft_result = this.number_convert / 304.8
                    }

                }
            },
        })
    </script>

@stop
