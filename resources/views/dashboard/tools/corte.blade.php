@extends('layouts.dashboard')

@section('content')

    <div id="app">
        <div class="container ">
            <div class="row justify-content-center">
                <div class="col-md-7">
                    <div class="card h-100">
                        <div class="card-header">
                            <strong>Cortes</strong>
                        </div>
                        <div class="card-body card-block">
                            <form class="form-horizontal">
                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Tamaño en Pulgadas</label>
                                    </div>
                                    <div class="col col-sm-5">
                                        <input type="text" v-model="width" v-on:keyup="calculatePrice"
                                               placeholder="Ancho"
                                               class="form-control">
                                    </div>
                                    <div class="col col-sm-5">
                                        <input type="text" v-model="height" v-on:keyup="calculatePrice"
                                               placeholder="Alto"
                                               class="form-control">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Material</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <select v-model="material_option" @change="calculatePrice" class="form-control">
                                            <option>Seleccionar Material</option>
                                            @foreach($itemOptions as $item)
                                                <option
                                                    value="{{ $item->option->value }}">{{ $item->option->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Pies cuadrados</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <input type="text" v-model="ft" v-on:keyup="calculatePrice"
                                               class="form-control">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Precio Corte</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <input type="text" v-model="price_tool"
                                               class="form-control"
                                               readonly>
                                    </div>
                                </div>


                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Precio diseño</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <input type="text" v-model="design_price"
                                               class="form-control"
                                               readonly>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Precio total</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <input type="text" v-model="price"
                                               class="form-control"
                                               readonly>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-md-5">
                    <div class="card h-100">
                        <div class="card-header">
                            <strong>Instalación</strong>
                        </div>
                        <div class="card-body card-block">
                            <div class="row form-group">
                                <div class="col col-sm-4">
                                    <label for="input-small" class=" form-control-label">Sector de instalación</label>
                                </div>
                                <div class="col col-sm-8">
                                    <select v-model="install_sector_option" @change="calculatePrice"
                                            class="form-control">
                                        <option>Seleccionar Sector</option>
                                        <option value="Local">Local</option>
                                        <option value="A">A</option>
                                        <option value="B">B</option>
                                        <option value="C">C</option>
                                        <option value="D">D</option>
                                        <option value="E">E</option>
                                        <option value="F">F</option>


                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-sm-4">
                                    <label for="input-small" class=" form-control-label">Precio instalación</label>
                                </div>
                                <div class="col col-sm-8">
                                    <input type="text" v-model="price_install"
                                           class="form-control"
                                           readonly>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-sm-4">
                                    <label for="input-small" class=" form-control-label">Precio con instalación</label>
                                </div>
                                <div class="col col-sm-8">
                                    <input type="text" v-model="price_with_install"
                                           class="form-control"
                                           readonly>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>


            <div class="row justify-content-center my-4">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Convertidor de Medidas a pies</strong>
                        </div>
                        <div class="card-body card-block">
                            <div class="row form-group">
                                <div class="col col-sm-2">
                                    <label for="input-small" class=" form-control-label">Numero a convertir</label>
                                </div>
                                <div class="col col-sm-10">
                                    <input type="text" v-on:keyup="calculateFt" v-model="number_convert"
                                           class="form-control">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-sm-2">
                                    <label for="input-small" class=" form-control-label">Unidad de medida</label>
                                </div>
                                <div class="col col-sm-10">
                                    <select v-model="unit_select" @change="calculateFt"
                                            class="form-control">
                                        <option>Seleccionar Unidad</option>
                                        <option value="Pulgadas">Pulgadas</option>
                                        <option value="Metros">Metros</option>
                                        <option value="Centimetros">Centimetros</option>
                                        <option value="Milimetros">Milimetros</option>


                                    </select>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-sm-2">
                                    <label for="input-small" class=" form-control-label">Resultado en pies</label>
                                </div>
                                <div class="col col-sm-10">
                                    <input type="text" v-model="ft_result"
                                           class="form-control" readonly>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Sectores de instalación para corte</strong>
                        </div>
                        <div class="card-body card-block">
                            <table class="table table-striped table-earning table-responsive-md">
                                <thead>
                                <tr>
                                    <th>A</th>
                                    <th>B</th>
                                    <th>C</th>
                                    <th>D</th>
                                    <th>E</th>
                                    <th>F</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $temp = 1;
                                @endphp

                                @foreach($tableOptions as $item)

                                    @php
                                        if($temp == 1) {
                                        echo '<tr>';
                                        }

                                    @endphp
                                    <td>{{ $item->option->name }}</td>
                                    @php
                                        $temp++;
                                        if($temp == 7) {
                                            echo '</tr>';
                                            $temp = 1;
                                        }

                                    @endphp
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>

    <script>
        var app = new Vue({
            el: '#app',
            data: {
                price: '',
                width: '',
                height: '',
                ft: '',
                dataPrice: '',
                material_option: 'Seleccionar Material',
                number_convert: '',
                unit_select: 'Seleccionar Unidad',
                ft_result: '',
                design_price: '',
                price_tool: '',
                sector_price: '',
                install_sector_option: 'Seleccionar Sector',
                price_install: '',
                price_with_install: '',
            },
            mounted() {
                axios
                    .get('/var/data/48')
                    .then(response => (this.sector_price = response.data))

            },
            methods: {
                calculatePrice: function () {

                    if (this.material_option != 'Seleccionar Material') {

                        this.ft = ((this.width * this.height) / 144).toFixed(2)

                        if (this.ft <= 1) {
                            this.price = 'No aplica a menos de 1 pie cuadrado.'
                        } else if(this.ft > 1 && this.ft <= 5 ) {
                            this.design_price = 'B/.' + 5
                            this.price = 'B/.' + ((this.ft * this.material_option) + 5).toFixed(2)
                            this.price_tool = 'B/.' + (this.ft * this.material_option).toFixed(2)
                        } else {
                            this.design_price = 'B/.' + parseInt(this.ft)
                            this.price = 'B/.' + ((this.ft * this.material_option) + parseInt(this.ft)).toFixed(2)
                            this.price_tool = 'B/.' + (this.ft * this.material_option).toFixed(2)
                        }
                    }


                    if (this.install_sector_option === 'A') {
                        this.price_install = 'B/.' + parseFloat(this.sector_price[0].value)
                    } else if (this.install_sector_option === 'B') {
                        this.price_install = 'B/.' + parseFloat(this.sector_price[1].value)
                    } else if (this.install_sector_option === 'C') {
                        this.price_install = 'B/.' + parseFloat(this.sector_price[2].value)
                    } else if (this.install_sector_option === 'D') {
                        this.price_install = 'B/.' + parseFloat(this.sector_price[3].value)
                    } else if (this.install_sector_option === 'E') {
                        this.price_install = 'B/.' + parseFloat(this.sector_price[4].value)
                    } else if (this.install_sector_option === 'F') {
                        this.price_install = 'B/.' + parseFloat(this.sector_price[5].value)
                    }else if (this.install_sector_option === 'Local') {
                        this.price_install = 'B/.0.00'
                    }

                    price_install_split = this.price_install.split("B/.");
                    price_split = this.price.split("B/.");

                    if (price_install_split.length > 1 && price_split.length > 1) {
                        this.price_with_install = 'B/.' + (parseFloat(price_install_split[1]) + parseFloat(price_split[1]));
                    }


                },



                calculateFt: function () {

                    if (this.unit_select === 'Pulgadas') {
                        this.ft_result = this.number_convert / 12
                    } else if (this.unit_select === 'Metros') {
                        this.ft_result = this.number_convert / 0.3048
                    } else if (this.unit_select === 'Centimetros') {
                        this.ft_result = this.number_convert / 30.48
                    } else if (this.unit_select === 'Milimetros') {
                        this.ft_result = this.number_convert / 304.8
                    }

                }
            },
        })
    </script>

@stop
