@extends('layouts.dashboard')

@section('content')

    <div id="app">
        <div class="container ">
            <div class="row mb-5">
                <div class="col-md-7">

                    <div class="card h-100">
                        <div class="card-header">
                            <strong>Facias</strong>
                        </div>
                        <div class="card-body card-block">
                            <form class="form-horizontal">
                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Tamaño en pies</label>
                                    </div>
                                    <div class="col col-sm-5">
                                        <input type="text" v-model="width" v-on:keyup="calculatePrice"
                                               placeholder="Ancho"
                                               class="form-control">
                                    </div>
                                    <div class="col col-sm-5">
                                        <input type="text" v-model="height" v-on:keyup="calculatePrice"
                                               placeholder="Alto"
                                               class="form-control">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Material</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <select v-model="material_option" @change="calculatePrice" class="form-control">
                                            <option>Seleccionar Material</option>
                                            @foreach($itemOptions as $item)
                                                <option
                                                    value="{{ $item->option->value }}">{{ $item->option->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Estructura</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <select v-model="structure" @change="calculatePrice" class="form-control">
                                            <option value="SI">SI</option>
                                            <option value="NO">NO</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Impresión</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <select v-model="print" @change="calculatePrice" class="form-control">
                                            <option value="SI">SI</option>
                                            <option value="NO">NO</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Lata</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <select v-model="can" @change="calculatePrice" class="form-control">
                                            <option value="SI">SI</option>
                                            <option value="NO">NO</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Laminado</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <select v-model="laminado" @change="calculatePrice" class="form-control">
                                            <option value="SI">SI</option>
                                            <option value="NO">NO</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Precio sin
                                            instalación</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <input type="text" v-model="price_no_install"
                                               class="form-control"
                                               readonly>
                                    </div>
                                </div>


                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-md-5">
                    <div class="card h-100">
                        <div class="card-header">
                            <strong>Instalación</strong>
                        </div>
                        <div class="card-body card-block">
                            <div class="row form-group">
                                <div class="col col-sm-4">
                                    <label for="input-small" class=" form-control-label">Sector de instalación</label>
                                </div>
                                <div class="col col-sm-8">
                                    <select v-model="install_sector_option" @change="calculatePrice"
                                            class="form-control">
                                        <option>Seleccionar Sector</option>
                                        <option value="A">A</option>
                                        <option value="B">B</option>
                                        <option value="C">C</option>
                                        <option value="D">D</option>
                                        <option value="E">E</option>


                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-sm-4">
                                    <label for="input-small" class=" form-control-label">Precio instalación</label>
                                </div>
                                <div class="col col-sm-8">
                                    <input type="text" v-model="price_install"
                                           class="form-control"
                                           readonly>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-sm-4">
                                    <label for="input-small" class=" form-control-label">Precio con instalación</label>
                                </div>
                                <div class="col col-sm-8">
                                    <input type="text" v-model="price_with_install"
                                           class="form-control"
                                           readonly>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Convertidor de Medidas a pies</strong>
                        </div>
                        <div class="card-body card-block">
                            <div class="row form-group">
                                <div class="col col-sm-2">
                                    <label for="input-small" class=" form-control-label">Numero a convertir</label>
                                </div>
                                <div class="col col-sm-10">
                                    <input type="text" v-on:keyup="calculateFt" v-model="number_convert"
                                           class="form-control">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-sm-2">
                                    <label for="input-small" class=" form-control-label">Unidad de medida</label>
                                </div>
                                <div class="col col-sm-10">
                                    <select v-model="unit_select" @change="calculateFt"
                                            class="form-control">
                                        <option>Seleccionar Unidad</option>
                                        <option value="Pulgadas">Pulgadas</option>
                                        <option value="Metros">Metros</option>
                                        <option value="Centimetros">Centimetros</option>
                                        <option value="Milimetros">Milimetros</option>


                                    </select>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-sm-2">
                                    <label for="input-small" class=" form-control-label">Resultado en pies</label>
                                </div>
                                <div class="col col-sm-10">
                                    <input type="text" v-model="ft_result"
                                           class="form-control" readonly>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Sectores de instalación para toldas</strong>
                        </div>
                        <div class="card-body card-block">
                            <table class="table table-striped table-earning table-responsive-md">
                                <thead>
                                <tr>
                                    <th>A</th>
                                    <th>B</th>
                                    <th>C</th>
                                    <th>D</th>
                                    <th>E</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $temp = 1;
                                @endphp

                                @foreach($tableOptions as $item)

                                    @php
                                        if($temp == 1) {
                                        echo '<tr>';
                                        }

                                    @endphp
                                    <td>{{ $item->option->name }}</td>
                                    @php
                                        $temp++;
                                        if($temp == 6) {
                                            echo '</tr>';
                                            $temp = 1;
                                        }

                                    @endphp
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>

    <script>
        var app = new Vue({
            el: '#app',
            data: {
                price_no_install: '',
                price_install: '',
                structure: 'SI',
                print: 'SI',
                can: 'SI',
                laminado: 'SI',
                width: '',
                height: '',
                dataPrice: '',
                material_option: 'Seleccionar Material',
                install_sector_option: 'Seleccionar Sector',
                price_20_more_aluminio: '',
                price_20_more_hierro: '',
                price_20_less_aluminio: '',
                price_20_less_hierro: '',
                sector_15_less: '',
                sector_15_40: '',
                sector_40_more: '',

                number_convert: '',
                unit_select: 'Seleccionar Unidad',
                ft_result: '',
            },
            mounted() {

                axios.get('/var/data/42')
                    .then(response => (this.price_20_more_aluminio = response.data))


                axios.get('/var/data/43')
                    .then(response => (this.price_20_more_hierro = response.data))

                axios.get('/var/data/44')
                    .then(response => (this.price_20_less_aluminio = response.data))


                axios.get('/var/data/45')
                    .then(response => (this.price_20_less_hierro = response.data))


                axios.get('/var/data/39')
                    .then(response => (this.sector_15_less = response.data))


                axios.get('/var/data/40')
                    .then(response => (this.sector_15_40 = response.data))


                axios.get('/var/data/41')
                    .then(response => (this.sector_40_more = response.data))


            },
            methods: {
                calculatePrice: function () {


                    let ft = this.width * this.height
                    let total = 0

                    if (this.width > 20 && this.material_option === 'Aluminio') {

                        if (this.structure === 'SI') {
                            total = total + ft * this.price_20_more_aluminio[0].value
                        }

                        if (this.print === 'SI') {
                            total = total + ft * this.price_20_more_aluminio[1].value
                        }

                        if (this.can === 'SI') {
                            total = total + ft * this.price_20_more_aluminio[2].value
                        }

                        if (this.laminado === 'SI') {
                            total = total + ft * this.price_20_more_aluminio[3].value
                        }

                        this.price_no_install = 'B/.' + total
                    } else if (this.width > 20 && this.material_option === 'Hierro') {

                        if (this.structure === 'SI') {
                            total = total + ft * this.price_20_more_hierro[0].value
                        }

                        if (this.print === 'SI') {
                            total = total + ft * this.price_20_more_hierro[1].value
                        }

                        if (this.can === 'SI') {
                            total = total + ft * this.price_20_more_hierro[2].value
                        }

                        if (this.laminado === 'SI') {
                            total = total + ft * this.price_20_more_hierro[3].value
                        }

                        this.price_no_install = 'B/.' + total
                    } else if (this.width <= 20 && this.material_option === 'Aluminio') {

                        if (this.structure === 'SI') {
                            total = total + ft * this.price_20_less_aluminio[0].value
                        }

                        if (this.print === 'SI') {
                            total = total + ft * this.price_20_less_aluminio[1].value
                        }

                        if (this.can === 'SI') {
                            total = total + ft * this.price_20_less_aluminio[2].value
                        }

                        if (this.laminado === 'SI') {
                            total = total + ft * this.price_20_less_aluminio[3].value
                        }

                        this.price_no_install = 'B/.' + total
                    } else if (this.width <= 20 && this.material_option === 'Hierro') {

                        if (this.structure === 'SI') {
                            total = total + ft * this.price_20_less_hierro[0].value
                        }

                        if (this.print === 'SI') {
                            total = total + ft * this.price_20_less_hierro[1].value
                        }

                        if (this.can === 'SI') {
                            total = total + ft * this.price_20_less_hierro[2].value
                        }

                        if (this.laminado === 'SI') {
                            total = total + ft * this.price_20_less_hierro[3].value
                        }

                        this.price_no_install = 'B/.' + total
                    }


                    sector = ['A', 'B', 'C', 'D', 'E']

                    for (i = 0; i < sector.length; i++) {
                        if (this.width < 15 && this.install_sector_option === sector[i]) {
                            this.price_install = 'B/.' + this.sector_15_less[i].value
                        } else if (this.width >= 15 && this.width <= 40 && this.install_sector_option === sector[i]) {
                            this.price_install = 'B/.' + this.sector_15_40[i].value
                        } else if (this.width > 40 && this.install_sector_option === sector[i]) {
                            this.price_install = 'B/.' + this.sector_40_more[i].value
                        }
                    }

                    price_install_split = this.price_install.split("B/.");
                    if (price_install_split.length > 1) {
                        this.price_with_install = 'B/.' + (parseFloat(price_install_split[1]) + parseFloat(total));
                    }


                },

                calculateFt: function () {

                    if (this.unit_select === 'Pulgadas') {
                        this.ft_result = this.number_convert / 12
                    } else if (this.unit_select === 'Metros') {
                        this.ft_result = this.number_convert / 0.3048
                    } else if (this.unit_select === 'Centimetros') {
                        this.ft_result = this.number_convert / 30.48
                    } else if (this.unit_select === 'Milimetros') {
                        this.ft_result = this.number_convert / 304.8
                    }

                }

            }
        })
    </script>

@stop
