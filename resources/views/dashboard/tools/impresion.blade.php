@extends('layouts.dashboard')

@section('content')

    <div id="app">
        <div class="container ">
            <div class="row justify-content-center">
                <div class="col-md-10">

                    <div class="card">
                        <div class="card-header">
                            <strong>Impresion</strong>
                        </div>
                        <div class="card-body card-block">
                            <form class="form-horizontal">
                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Tamaño en pies</label>
                                    </div>
                                    <div class="col col-sm-5">
                                        <input type="text" v-model="width" v-on:keyup="calculatePrice"
                                               placeholder="Largo"
                                               class="form-control">
                                    </div>
                                    <div class="col col-sm-5">
                                        <input type="text" v-model="height" v-on:keyup="calculatePrice"
                                               placeholder="Alto"
                                               class="form-control">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Material</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <select v-model="material_option" @change="calculatePrice" class="form-control">
                                            <option>Seleccionar Material</option>
                                            @foreach($itemOptions as $item)
                                                <option
                                                    value="{{ $item->option->value }}">{{ $item->option->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Equipo</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <select v-model="equip" @change="calculatePrice" class="form-control">
                                            <option>Seleccionar Equipo</option>
                                            @foreach($equips as $item)
                                                <option
                                                    value="{{ $item->option->value }}">{{ $item->option->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Pies Cuadrados</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <input type="text" v-model="ft" v-on:keyup="calculatePrice"
                                               class="form-control" readonly>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Laminado</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <select v-model="laminado" @change="calculatePrice" class="form-control">
                                            <option>Seleccionar Laminado</option>
                                            @foreach($metalics as $item)
                                                <option
                                                    value="{{ $item->option->value }}">{{ $item->option->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Precio Total</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <input type="text" v-model="total_price"
                                               class="form-control"
                                               readonly>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-sm-12 text-danger">
                                        <span><b>El precio no incluye Instalación.</b> </span>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row justify-content-center">
                <div class="col-md-10">
                    <div class="card">
                        <div class="card-header">
                            <strong>Convertidor de Medidas a pies</strong>
                        </div>
                        <div class="card-body card-block">
                            <div class="row form-group">
                                <div class="col col-sm-2">
                                    <label for="input-small" class=" form-control-label">Numero a convertir</label>
                                </div>
                                <div class="col col-sm-10">
                                    <input type="text" v-on:keyup="calculateFt" v-model="number_convert"
                                           class="form-control">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-sm-2">
                                    <label for="input-small" class=" form-control-label">Unidad de medida</label>
                                </div>
                                <div class="col col-sm-10">
                                    <select v-model="unit_select" @change="calculateFt"
                                            class="form-control">
                                        <option>Seleccionar Unidad</option>
                                        <option value="Pulgadas">Pulgadas</option>
                                        <option value="Metros">Metros</option>
                                        <option value="Centimetros">Centimetros</option>
                                        <option value="Milimetros">Milimetros</option>


                                    </select>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-sm-2">
                                    <label for="input-small" class=" form-control-label">Resultado en pies</label>
                                </div>
                                <div class="col col-sm-10">
                                    <input type="text" v-model="ft_result"
                                           class="form-control" readonly>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>

    <script>
        var app = new Vue({
            el: '#app',
            data: {
                width: '',
                height: '',
                ft: '',
                total_price: '',
                material_option: 'Seleccionar Material',
                equip: 'Seleccionar Equipo',
                laminado: 'Seleccionar Laminado',
                all_materials: '',
                times: '',
                mimaki: '',
                dgi: '',
                ricoh: '',
                laminado_price: '',
                number_convert: '',
                unit_select: 'Seleccionar Unidad',
                ft_result: '',

            },
            mounted() {

                axios
                    .get('/var/data/31')
                    .then(response => (this.all_materials = response.data))
                axios
                    .get('/var/data/33')
                    .then(response => (this.times = response.data))

                axios
                    .get('/var/data/34')
                    .then(response => (this.mimaki = response.data))

                axios
                    .get('/var/data/35')
                    .then(response => (this.dgi = response.data))

                axios
                    .get('/var/data/36')
                    .then(response => (this.ricoh = response.data))

                axios
                    .get('/var/data/46')
                    .then(response => (this.laminado_price = response.data))


            },
            methods: {
                calculatePrice: function () {

                    this.ft = this.width * this.height
                    let value = 0

                    for (i = 0; i < this.all_materials.length; i++) {
                        if (this.material_option === this.all_materials[i].value && this.equip === 'Times') {
                            value = this.ft * this.times[i].value
                        } else if (this.material_option === this.all_materials[i].value && this.equip === 'Mimaki') {
                            value = this.ft * this.mimaki[i].value
                        } else if (this.material_option === this.all_materials[i].value && this.equip === 'DGI') {
                            value = this.ft * this.dgi[i].value
                        } else if (this.material_option === this.all_materials[i].value && this.equip === 'Ricoh') {
                            value = this.ft * this.ricoh[i].value
                        }
                    }


                    if (this.laminado === 'Sin Laminado') {
                        this.total_price = 'B/.' + parseFloat(value)
                    } else if (this.laminado === 'Laminado 8519  5 años 3 M') {
                        this.total_price =  'B/.' + (parseFloat(value) + (this.ft * parseFloat(this.laminado_price[1].value)))
                    } else if (this.laminado === 'Laminado de 3 años LG') {
                        this.total_price =  'B/.' + (parseFloat(value) + (this.ft *  parseFloat(this.laminado_price[2].value)))
                    } else if (this.laminado === 'Laminado de piso floorgraphic') {
                        this.total_price =  'B/.' + (parseFloat(value) + (this.ft *  parseFloat(this.laminado_price[3].value)))
                    }

                },

                calculateFt: function () {

                    if (this.unit_select === 'Pulgadas') {
                        this.ft_result = this.number_convert / 12
                    } else if (this.unit_select === 'Metros') {
                        this.ft_result = this.number_convert / 0.3048
                    } else if (this.unit_select === 'Centimetros') {
                        this.ft_result = this.number_convert / 30.48
                    } else if (this.unit_select === 'Milimetros') {
                        this.ft_result = this.number_convert / 304.8
                    }

                }
            },
        })
    </script>

@stop
