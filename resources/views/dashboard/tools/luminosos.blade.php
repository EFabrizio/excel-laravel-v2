@extends('layouts.dashboard')

@section('content')

    <div id="app">
        <div class="container ">
            <div class="row my-5">
                <div class="col-md-7">
                    <div class="card h-100">
                        <div class="card-header">
                            <strong>Luminosos</strong>
                        </div>
                        <div class="card-body card-block">
                            <form class="form-horizontal">
                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Tamaño en pies</label>
                                    </div>
                                    <div class="col col-sm-5">
                                        <input type="text" v-model="width" v-on:keyup="calculatePrice"
                                               placeholder="Ancho"
                                               class="form-control">
                                    </div>
                                    <div class="col col-sm-5">
                                        <input type="text" v-model="height" v-on:keyup="calculatePrice"
                                               placeholder="Alto"
                                               class="form-control">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Material</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <select v-model="material_option" @change="calculatePrice" class="form-control">
                                            <option>Seleccionar Material</option>
                                            @foreach($itemOptions as $item)
                                                <option
                                                    value="{{ $item->option->value }}">{{ $item->option->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Iluminación</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <select v-model="light" @change="calculatePrice" class="form-control">
                                            <option>Seleccionar Iluminación</option>
                                            @foreach($electric as $item)
                                                <option
                                                    value="{{ $item->option->value }}">{{ $item->option->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Caras y Poste</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <select v-model="faces" @change="calculatePrice" class="form-control">
                                            <option>Seleccionar Caras y Poste</option>
                                            @foreach($faces as $item)
                                                <option
                                                    value="{{ $item->option->value }}">{{ $item->option->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Precio sin
                                            instalación</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <input type="text" v-model="price_no_install"
                                               class="form-control"
                                               readonly>
                                    </div>
                                </div>


                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-md-5">
                    <div class="card h-100">
                        <div class="card-header">
                            <strong>Instalación</strong>
                        </div>
                        <div class="card-body card-block">
                            <div class="row form-group">
                                <div class="col col-sm-4">
                                    <label for="input-small" class=" form-control-label">Sector de instalación</label>
                                </div>
                                <div class="col col-sm-8">
                                    <select v-model="install_sector_option" @change="calculatePrice"
                                            class="form-control">
                                        <option>Seleccionar Sector</option>
                                        <option value="A">A</option>
                                        <option value="B">B</option>
                                        <option value="C">C</option>
                                        <option value="D">D</option>
                                        <option value="E">E</option>


                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-sm-4">
                                    <label for="input-small" class=" form-control-label">Precio instalación</label>
                                </div>
                                <div class="col col-sm-8">
                                    <input type="text" v-model="price_install"
                                           class="form-control"
                                           readonly>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-sm-4">
                                    <label for="input-small" class=" form-control-label">Precio con instalación</label>
                                </div>
                                <div class="col col-sm-8">
                                    <input type="text" v-model="price_with_install"
                                           class="form-control"
                                           readonly>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Convertidor de Medidas a pies</strong>
                        </div>
                        <div class="card-body card-block">
                            <div class="row form-group">
                                <div class="col col-sm-2">
                                    <label for="input-small" class=" form-control-label">Numero a convertir</label>
                                </div>
                                <div class="col col-sm-10">
                                    <input type="text" v-on:keyup="calculateFt" v-model="number_convert"
                                           class="form-control">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-sm-2">
                                    <label for="input-small" class=" form-control-label">Unidad de medida</label>
                                </div>
                                <div class="col col-sm-10">
                                    <select v-model="unit_select" @change="calculateFt"
                                            class="form-control">
                                        <option>Seleccionar Unidad</option>
                                        <option value="Pulgadas">Pulgadas</option>
                                        <option value="Metros">Metros</option>
                                        <option value="Centimetros">Centimetros</option>
                                        <option value="Milimetros">Milimetros</option>


                                    </select>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-sm-2">
                                    <label for="input-small" class=" form-control-label">Resultado en pies</label>
                                </div>
                                <div class="col col-sm-10">
                                    <input type="text" v-model="ft_result"
                                           class="form-control" readonly>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Sectores de instalación para toldas</strong>
                        </div>
                        <div class="card-body card-block">
                            <table class="table table-striped table-earning table-responsive-md">
                                <thead>
                                <tr>
                                    <th>A</th>
                                    <th>B</th>
                                    <th>C</th>
                                    <th>D</th>
                                    <th>E</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $temp = 1;
                                @endphp

                                @foreach($tableOptions as $item)

                                    @php
                                        if($temp == 1) {
                                        echo '<tr>';
                                        }

                                    @endphp
                                    <td>{{ $item->option->name }}</td>
                                    @php
                                        $temp++;
                                        if($temp == 6) {
                                            echo '</tr>';
                                            $temp = 1;
                                        }

                                    @endphp
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>

    <script>
        var app = new Vue({
            el: '#app',
            data: {
                price_no_install: '',
                price_install: '',
                light: 'Seleccionar Iluminación',
                faces: 'Seleccionar Caras y Poste',
                width: '',
                height: '',
                dataPrice: '',
                sectorPrice: '',
                material_option: 'Seleccionar Material',
                install_sector_option: 'Seleccionar Sector',
                ft_prices: '',
                poste: '',
                poste_15_max: '',
                poste_15_infinite: '',

                number_convert: '',
                unit_select: 'Seleccionar Unidad',
                ft_result: '',
            },
            mounted() {
                axios.get('/var/data/10')
                    .then(response => (this.ft_prices = response.data))

                axios.get('/var/data/11')
                    .then(response => (this.poste = response.data))

                axios.get('/var/data/12')
                    .then(response => (this.poste_15_max = response.data))

                axios.get('/var/data/13')
                    .then(response => (this.poste_15_infinite = response.data))

            },
            methods: {
                calculatePrice: function () {


                    if(this.height > 5) {
                        this.price_no_install = 'Error: Altura máxima 5 pies'
                    }

                    if (this.material_option === 'Hierro' && this.light === 'Con luz') {
                        this.price_no_install = 'Con luz solo aplica el aluminio'
                    } else if (this.width <= 4 && this.height <= 4 && this.material_option === 'Aluminio' && this.light === 'Con luz' && this.faces === '2 caras con poste') {
                        this.price_no_install = 'B/.' + this.ft_prices[0].value * this.width * this.height
                    } else if (this.width <= 4 && this.height <= 5 && this.material_option === 'Hierro' && this.light === 'Sin luz' && this.faces === '2 caras con poste') {
                        this.price_no_install = 'B/.' + this.ft_prices[1].value * this.width * this.height
                    } else if (this.width >= 4 && this.height >= 4 && this.material_option === 'Aluminio' && this.light === 'Con luz' && this.faces === '2 caras con poste') {
                        this.price_no_install = 'B/.' + this.ft_prices[2].value * this.width * this.height
                    } else if (this.width > 4 && this.width <= 10 && this.height <= 5 && this.material_option === 'Hierro' && this.light === 'Sin luz' && this.faces === '2 caras con poste') {
                        this.price_no_install = 'B/.' + this.ft_prices[3].value * this.width * this.height
                    } else if (this.width > 4 && this.width <= 10 && this.height <= 5 && this.material_option === 'Hierro' && this.light === 'Sin luz' && this.faces === '1 cara sin poste') {
                        this.price_no_install = 'B/.' + this.ft_prices[4].value * this.width * this.height
                    } else if (this.width <= 12 && this.height <= 5 && this.material_option === 'Aluminio' && this.light === 'Con luz' && this.faces === '1 cara sin poste') {
                        this.price_no_install = 'B/.' + this.ft_prices[5].value * this.width * this.height
                    } else if (this.width <= 10 && this.height <= 5 && this.material_option === 'Aluminio' && this.light === 'Sin luz' && this.faces === '1 cara sin poste') {
                        this.price_no_install = 'B/.' + this.ft_prices[6].value * this.width * this.height
                    } else if (this.width > 10 && this.height <= 5 && this.material_option === 'Hierro' && this.light === 'Sin luz' && this.faces === '1 cara sin poste') {
                        this.price_no_install = 'B/.' + this.ft_prices[7].value * this.width * this.height
                    } else if (this.width > 12 && this.height <= 5 && this.material_option === 'Aluminio' && this.light === 'Con luz' && this.faces === '1 cara sin poste') {
                        this.price_no_install = 'B/.' + this.ft_prices[9].value * this.width * this.height
                    } else if (this.width > 10 && this.height <= 5 && this.material_option === 'Aluminio' && this.light === 'Sin luz' && this.faces === '1 cara sin poste') {
                        this.price_no_install = 'B/.' + this.ft_prices[10].value * this.width * this.height
                    } else if (this.width > 20 && this.faces === '2 caras con poste') {
                        this.price_no_install = 'Muy grande para poste'
                    } else {
                        this.price_no_install = 'No aplica o faltan valores'
                    }


                    sector = ['A', 'B', 'C', 'D', 'E']

                    for (i = 0; i < sector.length; i++) {
                        if (this.faces === '2 caras con poste' && this.install_sector_option === sector[i]) {
                            this.price_install = 'B/.' + this.poste[i].value
                        } else if (this.faces === '1 cara sin poste' && this.install_sector_option === sector[i] && this.width <= 15) {
                            this.price_install = 'B/.' + this.poste_15_max[i].value
                        } else if (this.faces === '1 cara sin poste' && this.install_sector_option === sector[i] && this.width > 15) {
                            this.price_install = 'B/.' + this.poste_15_infinite[i].value
                        }
                    }

                    price_no_install_split = this.price_no_install.split("B/.");
                    price_install_split = this.price_install.split("B/.");
                    if (price_no_install_split.length > 1 && price_install_split.length > 1) {
                        this.price_with_install = 'B/.' + (parseFloat(price_no_install_split[1]) + parseFloat(price_install_split[1]));
                    }

                },


                calculateFt: function () {

                    if (this.unit_select === 'Pulgadas') {
                        this.ft_result = this.number_convert / 12
                    } else if (this.unit_select === 'Metros') {
                        this.ft_result = this.number_convert / 0.3048
                    } else if (this.unit_select === 'Centimetros') {
                        this.ft_result = this.number_convert / 30.48
                    } else if (this.unit_select === 'Milimetros') {
                        this.ft_result = this.number_convert / 304.8
                    }

                }

            }
        })
    </script>

@stop
