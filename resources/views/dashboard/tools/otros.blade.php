@extends('layouts.dashboard')

@section('content')

    <div id="app">
        <div class="container ">
            <div class="row justify-content-center">
                <div class="col-md-10">

                    <div class="card">
                        <div class="card-header">
                            <strong>Otros</strong>
                        </div>
                        <div class="card-body card-block">
                            <form class="form-horizontal">
                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Unidades</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <input type="text" v-on:keyup="calculatePrice" v-model="units"
                                               class="form-control">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Material</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <select v-model="material_option" @change="calculatePrice" class="form-control">
                                            <option>Seleccionar Material</option>
                                            @foreach($itemOptions as $item)
                                                <option
                                                    value="{{ $item->option->value }}">{{ $item->option->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Precio Total</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <input type="text" v-model="total_price"
                                               class="form-control"
                                               readonly>
                                    </div>
                                </div>


                            </form>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row justify-content-center">
                <div class="col-md-10">
                    <div class="card">
                        <div class="card-header">
                            <strong>Convertidor de Medidas a pies</strong>
                        </div>
                        <div class="card-body card-block">
                            <div class="row form-group">
                                <div class="col col-sm-2">
                                    <label for="input-small" class=" form-control-label">Numero a convertir</label>
                                </div>
                                <div class="col col-sm-10">
                                    <input type="text" v-on:keyup="calculateFt" v-model="number_convert"
                                           class="form-control">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-sm-2">
                                    <label for="input-small" class=" form-control-label">Unidad de medida</label>
                                </div>
                                <div class="col col-sm-10">
                                    <select v-model="unit_select" @change="calculateFt"
                                            class="form-control">
                                        <option>Seleccionar Unidad</option>
                                        <option value="Pulgadas">Pulgadas</option>
                                        <option value="Metros">Metros</option>
                                        <option value="Centimetros">Centimetros</option>
                                        <option value="Milimetros">Milimetros</option>


                                    </select>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-sm-2">
                                    <label for="input-small" class=" form-control-label">Resultado en pies</label>
                                </div>
                                <div class="col col-sm-10">
                                    <input type="text" v-model="ft_result"
                                           class="form-control" readonly>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>

    <script>
        var app = new Vue({
            el: '#app',
            data: {
                units: '',
                material_price: 'Seleccionar Material',
                material_price_normal: [],
                material_price_12: [],
                total_price: '',
                material_option: 'Seleccionar Material',
                number_convert: '',
                unit_select: 'Seleccionar Unidad',
                ft_result: '',

            },
            mounted() {
                axios
                    .get('/var/data/54')
                    .then(response => (this.material_price_normal = response.data))

                axios
                    .get('/var/data/55')
                    .then(response => (this.material_price_12 = response.data))


            },
            methods: {
                calculatePrice: function () {

                    for (i = 0; i < this.material_price_normal.length; i++) {

                        if (this.units < 12) {
                            if (this.material_option === this.material_price_normal[i].name) {
                                this.total_price = 'B/.' + (this.material_price_normal[i].value * this.units).toFixed(2)
                            }
                        } else {
                            if (this.material_option === this.material_price_12[i].name) {
                                this.total_price = 'B/.' + (this.material_price_12[i].value * this.units).toFixed(2)
                            }
                        }
                    }

                },

                calculateFt: function () {

                    if (this.unit_select === 'Pulgadas') {
                        this.ft_result = this.number_convert / 12
                    } else if (this.unit_select === 'Metros') {
                        this.ft_result = this.number_convert / 0.3048
                    } else if (this.unit_select === 'Centimetros') {
                        this.ft_result = this.number_convert / 30.48
                    } else if (this.unit_select === 'Milimetros') {
                        this.ft_result = this.number_convert / 304.8
                    }

                }
            },
        })
    </script>

@stop
