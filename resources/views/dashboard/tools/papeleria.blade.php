@extends('layouts.dashboard')

@section('content')

    <div id="app">
        <div class="container ">
            <div class="row justify-content-center">
                <div class="col-md-12">

                    <div class="card">
                        <div class="card-header">
                            <strong>CARTULINAS, PAPELES,SULFITOS Y PLASTIFICADOS</strong>
                        </div>
                        <div class="card-body card-block">
                            <form class="form-horizontal">
                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Cantidad</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <input type="text" v-model="quantity_one" v-on:keyup="calculatePrice"
                                               class="form-control">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Material</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <select v-model="material_option" @change="calculatePrice" class="form-control">
                                            <option>Seleccionar Material</option>
                                            @foreach($itemOptions as $item)
                                                <option
                                                    value="{{ $item->option->value }}">{{ $item->option->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Caras</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <select v-model="faces" @change="calculatePrice" class="form-control">
                                            <option>Seleccionar Caras</option>
                                            @foreach($faces as $item)
                                                <option
                                                    value="{{ $item->option->value }}">{{ $item->option->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Precio por
                                            impresion</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <input type="text" v-model="print_price_one"
                                               class="form-control"
                                               readonly>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Laminado</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <select v-model="laminado" @change="calculatePrice" class="form-control">
                                            <option>Seleccionar Laminado</option>
                                            @foreach($laminado as $item)
                                                <option
                                                    value="{{ $item->option->value }}">{{ $item->option->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Precio del laminado</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <input type="text" v-model="price_laminado"
                                               class="form-control"
                                               readonly>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Total de impresión con
                                            laminado</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <input type="text" v-model="total_printing"
                                               class="form-control"
                                               readonly>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-sm-12 text-danger">
                                        <span><b>Esta todo calculado en base a hojas 12 x 18.</b> </span>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row justify-content-center">
                <div class="col-md-12">

                    <div class="card">
                        <div class="card-header">
                            <strong>PAPEL ADHESIVO Y TROQUELADO</strong>
                        </div>
                        <div class="card-body card-block">
                            <form class="form-horizontal">
                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Cantidad</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <input type="text" v-model="quantity_two" v-on:keyup="calculatePrice"
                                               class="form-control">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Material</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <select v-model="material_two" @change="calculatePrice" class="form-control">
                                            <option>Seleccionar Material</option>
                                            @foreach($itemsTwo as $item)
                                                <option
                                                    value="{{ $item->option->value }}">{{ $item->option->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Precio por
                                            impresion</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <input type="text" v-model="print_price_two"
                                               class="form-control"
                                               readonly>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Troquelado</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <select v-model="troquelado" @change="calculatePrice" class="form-control">
                                            <option>Seleccionar Troquelado</option>
                                            @foreach($troquelado as $item)
                                                <option
                                                    value="{{ $item->option->value }}">{{ $item->option->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Precio del
                                            troquelado</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <input type="text" v-model="troquelado_price"
                                               class="form-control"
                                               readonly>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Total de impresión
                                            troquelada</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <input type="text" v-model="total_printing_two"
                                               class="form-control"
                                               readonly>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-sm-12 text-danger">
                                        <span><b>Esta todo calculado en base a hojas 12 x 18.</b> </span>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Convertidor de Medidas a pies</strong>
                        </div>
                        <div class="card-body card-block">
                            <div class="row form-group">
                                <div class="col col-sm-2">
                                    <label for="input-small" class=" form-control-label">Numero a convertir</label>
                                </div>
                                <div class="col col-sm-10">
                                    <input type="text" v-on:keyup="calculateFt" v-model="number_convert"
                                           class="form-control">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-sm-2">
                                    <label for="input-small" class=" form-control-label">Unidad de medida</label>
                                </div>
                                <div class="col col-sm-10">
                                    <select v-model="unit_select" @change="calculateFt"
                                            class="form-control">
                                        <option>Seleccionar Unidad</option>
                                        <option value="Pulgadas">Pulgadas</option>
                                        <option value="Metros">Metros</option>
                                        <option value="Centimetros">Centimetros</option>
                                        <option value="Milimetros">Milimetros</option>


                                    </select>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-sm-2">
                                    <label for="input-small" class=" form-control-label">Resultado en pies</label>
                                </div>
                                <div class="col col-sm-10">
                                    <input type="text" v-model="ft_result"
                                           class="form-control" readonly>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>

    <script>
        var app = new Vue({
            el: '#app',
            data: {
                price_no_install: '',
                quantity_one: '',
                quantity_two: '',
                material_option: 'Seleccionar Material',
                faces: 'Seleccionar Caras',
                laminado: 'Seleccionar Laminado',
                material_two: 'Seleccionar Material',
                troquelado: 'Seleccionar Troquelado',
                print_price_one: '',
                print_price_two: '',
                price_laminado: '',
                total_printing: '',
                troquelado_price: '',
                total_printing_two: '',

                // Faces Prices
                one_face_1_50: '',
                one_face_51_250: '',
                one_face_251_500: '',
                one_face_500: '',
                two_face_1_50: '',
                two_face_51_250: '',
                two_face_251_500: '',
                two_face_500: '',
                laminado_all_prices: '',
                prices_adhesive: '',
                load_prices_troquelado_1_50: '',
                load_prices_troquelado_50_500: '',
                number_convert: '',
                unit_select: 'Seleccionar Unidad',
                ft_result: '',
            },
            mounted() {
                axios
                    .get('/var/data/19')
                    .then(response => (this.one_face_1_50 = response.data))

                axios
                    .get('/var/data/20')
                    .then(response => (this.one_face_51_250 = response.data))
                axios
                    .get('/var/data/21')
                    .then(response => (this.one_face_251_500 = response.data))
                axios
                    .get('/var/data/22')
                    .then(response => (this.one_face_500 = response.data))

                axios
                    .get('/var/data/23')
                    .then(response => (this.two_face_1_50 = response.data))

                axios
                    .get('/var/data/24')
                    .then(response => (this.two_face_51_250 = response.data))
                axios
                    .get('/var/data/25')
                    .then(response => (this.two_face_251_500 = response.data))
                axios
                    .get('/var/data/26')
                    .then(response => (this.two_face_500 = response.data))

                axios
                    .get('/var/data/27')
                    .then(response => (this.laminado_all_prices = response.data))

                axios
                    .get('/var/data/28')
                    .then(response => (this.prices_adhesive = response.data))

                axios
                    .get('/var/data/29')
                    .then(response => (this.load_prices_troquelado_1_50 = response.data))

                axios
                    .get('/var/data/30')
                    .then(response => (this.load_prices_troquelado_50_500 = response.data))



            },
            methods: {
                calculatePrice: function () {

                    if (this.material_option === 'Cartulina perlada' && this.faces === 'Una cara' && this.quantity_one <= 50) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.one_face_1_50[0].value
                    } else if (this.material_option === 'Cartulina satinada' && this.faces === 'Una cara' && this.quantity_one <= 50) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.one_face_1_50[1].value
                    } else if (this.material_option === 'Cartulina espejo' && this.faces === 'Una cara' && this.quantity_one <= 50) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.one_face_1_50[2].value
                    } else if (this.material_option === 'Papel satinado' && this.faces === 'Una cara' && this.quantity_one <= 50) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.one_face_1_50[3].value
                    } else if (this.material_option === 'Sulfito' && this.faces === 'Una cara' && this.quantity_one <= 50) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.one_face_1_50[4].value
                    } else if (this.material_option === 'Cartulina perlada' && this.faces === 'Una cara' && this.quantity_one > 50 && this.quantity_one <= 250) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.one_face_51_250[0].value
                    } else if (this.material_option === 'Cartulina satinada' && this.faces === 'Una cara' && this.quantity_one > 50 && this.quantity_one <= 250) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.one_face_51_250[1].value
                    } else if (this.material_option === 'Cartulina espejo' && this.faces === 'Una cara' && this.quantity_one > 50 && this.quantity_one <= 250) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.one_face_51_250[2].value
                    } else if (this.material_option === 'Papel satinado' && this.faces === 'Una cara' && this.quantity_one > 50 && this.quantity_one <= 250) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.one_face_51_250[3].value
                    } else if (this.material_option === 'Sulfito' && this.faces === 'Una cara' && this.quantity_one > 50 && this.quantity_one <= 250) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.one_face_51_250[4].value
                    } else if (this.material_option === 'Cartulina perlada' && this.faces === 'Una cara' && this.quantity_one > 250 && this.quantity_one <= 500) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.one_face_251_500[0].value
                    } else if (this.material_option === 'Cartulina satinada' && this.faces === 'Una cara' && this.quantity_one > 250 && this.quantity_one <= 500) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.one_face_251_500[1].value
                    } else if (this.material_option === 'Cartulina espejo' && this.faces === 'Una cara' && this.quantity_one > 250 && this.quantity_one <= 500) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.one_face_251_500[2].value
                    } else if (this.material_option === 'Papel satinado' && this.faces === 'Una cara' && this.quantity_one > 250 && this.quantity_one <= 500) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.one_face_251_500[3].value
                    } else if (this.material_option === 'Sulfito' && this.faces === 'Una cara' && this.quantity_one > 250 && this.quantity_one <= 500) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.one_face_251_500[4].value
                    } else if (this.material_option === 'Cartulina perlada' && this.faces === 'Una cara' && this.quantity_one > 500) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.one_face_500[0].value
                    } else if (this.material_option === 'Cartulina satinada' && this.faces === 'Una cara' && this.quantity_one > 500) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.one_face_500[1].value
                    } else if (this.material_option === 'Cartulina espejo' && this.faces === 'Una cara' && this.quantity_one > 500) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.one_face_500[2].value
                    } else if (this.material_option === 'Papel satinado' && this.faces === 'Una cara' && this.quantity_one > 500) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.one_face_500[3].value
                    } else if (this.material_option === 'Sulfito' && this.faces === 'Una cara' && this.quantity_one > 500) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.one_face_500[4].value
                    } else if (this.material_option === 'Cartulina perlada' && this.faces === 'Dos caras' && this.quantity_one <= 50) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.two_face_1_50[0].value
                    } else if (this.material_option === 'Cartulina satinada' && this.faces === 'Dos caras' && this.quantity_one <= 50) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.two_face_1_50[1].value
                    } else if (this.material_option === 'Cartulina espejo' && this.faces === 'Dos caras' && this.quantity_one <= 50) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.two_face_1_50[2].value
                    } else if (this.material_option === 'Papel satinado' && this.faces === 'Dos caras' && this.quantity_one <= 50) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.two_face_1_50[3].value
                    } else if (this.material_option === 'Sulfito' && this.faces === 'Dos caras' && this.quantity_one <= 50) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.two_face_1_50[4].value
                    } else if (this.material_option === 'Cartulina perlada' && this.faces === 'Dos caras' && this.quantity_one > 50 && this.quantity_one <= 250) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.two_face_51_250[0].value
                    } else if (this.material_option === 'Cartulina satinada' && this.faces === 'Dos caras' && this.quantity_one > 50 && this.quantity_one <= 250) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.two_face_51_250[1].value
                    } else if (this.material_option === 'Cartulina espejo' && this.faces === 'Dos caras' && this.quantity_one > 50 && this.quantity_one <= 250) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.two_face_51_250[2].value
                    } else if (this.material_option === 'Papel satinado' && this.faces === 'Dos caras' && this.quantity_one > 50 && this.quantity_one <= 250) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.two_face_51_250[3].value
                    } else if (this.material_option === 'Sulfito' && this.faces === 'Dos caras' && this.quantity_one > 50 && this.quantity_one <= 250) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.two_face_51_250[4].value
                    } else if (this.material_option === 'Cartulina perlada' && this.faces === 'Dos caras' && this.quantity_one > 250 && this.quantity_one <= 500) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.two_face_251_500[0].value
                    } else if (this.material_option === 'Cartulina satinada' && this.faces === 'Dos caras' && this.quantity_one > 250 && this.quantity_one <= 500) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.two_face_251_500[1].value
                    } else if (this.material_option === 'Cartulina espejo' && this.faces === 'Dos caras' && this.quantity_one > 250 && this.quantity_one <= 500) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.two_face_251_500[2].value
                    } else if (this.material_option === 'Papel satinado' && this.faces === 'Dos caras' && this.quantity_one > 250 && this.quantity_one <= 500) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.two_face_251_500[3].value
                    } else if (this.material_option === 'Sulfito' && this.faces === 'Dos caras' && this.quantity_one > 250 && this.quantity_one <= 500) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.two_face_251_500[4].value
                    } else if (this.material_option === 'Cartulina perlada' && this.faces === 'Dos caras' && this.quantity_one > 500) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.two_face_500[0].value
                    } else if (this.material_option === 'Cartulina satinada' && this.faces === 'Dos caras' && this.quantity_one > 500) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.two_face_500[1].value
                    } else if (this.material_option === 'Cartulina espejo' && this.faces === 'Dos caras' && this.quantity_one > 500) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.two_face_500[2].value
                    } else if (this.material_option === 'Papel satinado' && this.faces === 'Dos caras' && this.quantity_one > 500) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.two_face_500[3].value
                    } else if (this.material_option === 'Sulfito' && this.faces === 'Dos caras' && this.quantity_one > 500) {
                        this.print_price_one = 'B/.' + this.quantity_one * this.two_face_500[4].value
                    }

                    if (this.laminado == 'No lleva') {
                        this.price_laminado = 'B/.0.00'
                    } else if (this.laminado == 'Mate') {
                        this.price_laminado = 'B/.' + this.laminado_all_prices[0].value * this.quantity_one
                    } else if (this.laminado == 'Brillante') {
                        this.price_laminado = 'B/.' + this.laminado_all_prices[1].value * this.quantity_one
                    } else if (this.laminado == 'Plastificado') {
                        this.price_laminado = 'B/.' + this.laminado_all_prices[2].value * this.quantity_one
                    }

                    print_price_one_split = this.print_price_one.split("B/.");
                    price_laminado_split = this.price_laminado.split("B/.");
                    if (print_price_one_split.length > 1 && price_laminado_split.length > 1) {
                        this.total_printing = 'B/.' + (parseFloat(print_price_one_split[1]) + parseFloat(price_laminado_split[1]))
                    }


                    if (this.material_two === 'Papel adhesivo brillante') {
                            this.print_price_two = 'B/.' + this.prices_adhesive[0].value * this.quantity_two
                    } else if (this.material_two === 'Papel adhesivo Mate') {
                        this.print_price_two = 'B/.' + this.prices_adhesive[1].value * this.quantity_two
                    }

                    if(this.troquelado === 'No lleva') {
                        this.troquelado_price = 'B/.0.00'
                    } else if(this.troquelado === 'Troquelado' && this.quantity_two <= 50) {
                        this.troquelado_price = 'B/.' + (this.quantity_two * this.load_prices_troquelado_1_50[0].value)
                    } else if(this.troquelado === 'Troquelado' && this.quantity_two > 50) {
                        this.troquelado_price = 'B/.' + (this.quantity_two * this.load_prices_troquelado_50_500[0].value)
                    }

                    print_price_two_split = this.print_price_two.split("B/.");
                    troquelado_price_split = this.troquelado_price.split("B/.");
                    if (troquelado_price_split.length > 1 && print_price_two_split.length > 1) {
                        this.total_printing_two = 'B/.' + (parseFloat(troquelado_price_split[1]) + parseFloat(print_price_two_split[1]))

                    }


                },

                calculateFt: function () {

                    if (this.unit_select === 'Pulgadas') {
                        this.ft_result = this.number_convert / 12
                    } else if (this.unit_select === 'Metros') {
                        this.ft_result = this.number_convert / 0.3048
                    } else if (this.unit_select === 'Centimetros') {
                        this.ft_result = this.number_convert / 30.48
                    } else if (this.unit_select === 'Milimetros') {
                        this.ft_result = this.number_convert / 304.8
                    }

                }
            },
        })
    </script>

@stop
