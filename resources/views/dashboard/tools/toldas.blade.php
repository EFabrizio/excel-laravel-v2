@extends('layouts.dashboard')

@section('content')

    <div id="app">
        <div class="container ">
            <div class="row mb-5">
                <div class="col-md-7">

                    <div class="card h-100">
                        <div class="card-header">
                            <strong>Toldas</strong>
                        </div>
                        <div class="card-body card-block">
                            <form class="form-horizontal">
                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Tamaño en pies</label>
                                    </div>
                                    <div class="col col-sm-5">
                                        <input type="text" v-model="width" v-on:keyup="calculatePrice"
                                               placeholder="Ancho"
                                               class="form-control">
                                    </div>
                                    <div class="col col-sm-5">
                                        <input type="text" v-model="height" v-on:keyup="calculatePrice"
                                               placeholder="Alto"
                                               class="form-control">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Material</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <select v-model="material_option" @change="calculatePrice" class="form-control">
                                            <option>Seleccionar Material</option>
                                            @foreach($itemOptions as $item)
                                                <option
                                                    value="{{ $item->option->value }}">{{ $item->option->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-sm-2">
                                        <label for="input-small" class=" form-control-label">Precio sin
                                            instalación</label>
                                    </div>
                                    <div class="col col-sm-10">
                                        <input type="text" v-model="price_no_install"
                                               class="form-control"
                                               readonly>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-sm-12 text-danger">
                                        <span><b>El precio no incluye Rotulacion ni servicio de pintura.</b> </span>
                                    </div>
                                </div>


                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-md-5">
                    <div class="card h-100">
                        <div class="card-header">
                            <strong>Instalación</strong>
                        </div>
                        <div class="card-body card-block">
                            <div class="row form-group">
                                <div class="col col-sm-4">
                                    <label for="input-small" class=" form-control-label">Sector de instalación</label>
                                </div>
                                <div class="col col-sm-8">
                                    <select v-model="install_sector_option" @change="calculatePrice"
                                            class="form-control">
                                        <option>Seleccionar Sector</option>
                                        <option value="A">A</option>
                                        <option value="B">B</option>
                                        <option value="C">C</option>
                                        <option value="D">D</option>
                                        <option value="E">E</option>


                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-sm-4">
                                    <label for="input-small" class=" form-control-label">Precio instalación</label>
                                </div>
                                <div class="col col-sm-8">
                                    <input type="text" v-model="price_install"
                                           class="form-control"
                                           readonly>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-sm-4">
                                    <label for="input-small" class=" form-control-label">Precio con instalación</label>
                                </div>
                                <div class="col col-sm-8">
                                    <input type="text" v-model="price_with_install"
                                           class="form-control"
                                           readonly>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Convertidor de Medidas a pies</strong>
                        </div>
                        <div class="card-body card-block">
                            <div class="row form-group">
                                <div class="col col-sm-2">
                                    <label for="input-small" class=" form-control-label">Numero a convertir</label>
                                </div>
                                <div class="col col-sm-10">
                                    <input type="text" v-on:keyup="calculateFt" v-model="number_convert"
                                           class="form-control">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-sm-2">
                                    <label for="input-small" class=" form-control-label">Unidad de medida</label>
                                </div>
                                <div class="col col-sm-10">
                                    <select v-model="unit_select" @change="calculateFt"
                                            class="form-control">
                                        <option>Seleccionar Unidad</option>
                                        <option value="Pulgadas">Pulgadas</option>
                                        <option value="Metros">Metros</option>
                                        <option value="Centimetros">Centimetros</option>
                                        <option value="Milimetros">Milimetros</option>


                                    </select>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-sm-2">
                                    <label for="input-small" class=" form-control-label">Resultado en pies</label>
                                </div>
                                <div class="col col-sm-10">
                                    <input type="text" v-model="ft_result"
                                           class="form-control" readonly>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Sectores de instalación para toldas</strong>
                        </div>
                        <div class="card-body card-block">
                            <table class="table table-striped table-earning table-responsive-md">
                                <thead>
                                <tr>
                                    <th>A</th>
                                    <th>B</th>
                                    <th>C</th>
                                    <th>D</th>
                                    <th>E</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $temp = 1;
                                @endphp

                                @foreach($tableOptions as $item)

                                    @php
                                        if($temp == 1) {
                                        echo '<tr>';
                                        }

                                    @endphp
                                    <td>{{ $item->option->name }}</td>
                                    @php
                                        $temp++;
                                        if($temp == 6) {
                                            echo '</tr>';
                                            $temp = 1;
                                        }

                                    @endphp
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>

    <script>
        var app = new Vue({
            el: '#app',
            data: {
                price_no_install: '',
                price_install: '',
                width: '',
                height: '',
                dataPrice: '',
                sectorPrice: '',
                material_option: 'Seleccionar Material',
                install_sector_option: 'Seleccionar Sector',
                price_with_install: '',
                number_convert: '',
                unit_select: 'Seleccionar Unidad',
                ft_result: '',
            },
            mounted() {
                axios
                    .get('/var/data/1')
                    .then(response => (this.dataPrice = response.data))

                axios
                    .get('/var/data/4')
                    .then(response => (this.sectorCheapPrice = response.data))

                axios
                    .get('/var/data/5')
                    .then(response => (this.sectorExpensivePrice = response.data))
            },
            methods: {
                calculatePrice: function () {

                    if (this.material_option != 'Seleccionar Material') {
                        if(this.height > 5) {
                            this.price_no_install = 'El máximo en altura es 5'
                        } else {
                            if (this.width <= 10 && this.material_option == 11) {
                                this.price_no_install = this.width * this.height * this.dataPrice[2].value
                            } else if (this.width <= 10 && this.material_option == 9.50) {
                                this.price_no_install = this.width * this.height * this.material_option
                            } else if (this.width > 10 && this.material_option == 11) {
                                this.price_no_install = this.width * this.height * this.material_option
                            } else {
                                this.price_no_install = this.width * this.height * this.dataPrice[3].value
                            }
                            if (this.width <= 15) {
                                if (this.install_sector_option === 'A') {
                                    this.price_install = 'B/.' + parseFloat(this.sectorCheapPrice[0].value)
                                } else if (this.install_sector_option === 'B') {
                                    this.price_install = 'B/.' + parseFloat(this.sectorCheapPrice[1].value)
                                } else if (this.install_sector_option === 'C') {
                                    this.price_install = 'B/.' + parseFloat(this.sectorCheapPrice[2].value)
                                } else if (this.install_sector_option === 'D') {
                                    this.price_install = 'B/.' + parseFloat(this.sectorCheapPrice[3].value)
                                } else if (this.install_sector_option === 'E') {
                                    this.price_install = 'B/.' + parseFloat(this.sectorCheapPrice[4].value)
                                }
                            } else {
                                if (this.install_sector_option === 'A') {
                                    this.price_install = 'B/.' + parseFloat(this.sectorExpensivePrice[0].value)
                                } else if (this.install_sector_option === 'B') {
                                    this.price_install = 'B/.' + parseFloat(this.sectorExpensivePrice[1].value)
                                } else if (this.install_sector_option === 'C') {
                                    this.price_install = 'B/.' + parseFloat(this.sectorExpensivePrice[2].value)
                                } else if (this.install_sector_option === 'D') {
                                    this.price_install = 'B/.' + parseFloat(this.sectorExpensivePrice[3].value)
                                } else if (this.install_sector_option === 'E') {
                                    this.price_install = 'B/.' + parseFloat(this.sectorExpensivePrice[4].value)
                                }
                            }

                            price_install_split = this.price_install.split("B/.");

                            if (price_install_split.length > 1) {
                                this.price_with_install = 'B/.' + (parseFloat(price_install_split[1]) + parseFloat(this.price_no_install));
                            }

                            this.price_no_install = 'B/.' + this.price_no_install;

                        }
                    }
                },

                calculateFt: function () {

                    if (this.unit_select === 'Pulgadas') {
                        this.ft_result = this.number_convert / 12
                    } else if (this.unit_select === 'Metros') {
                        this.ft_result = this.number_convert / 0.3048
                    } else if (this.unit_select === 'Centimetros') {
                        this.ft_result = this.number_convert / 30.48
                    } else if (this.unit_select === 'Milimetros') {
                        this.ft_result = this.number_convert / 304.8
                    }

                }

            }
        })
    </script>

@stop
