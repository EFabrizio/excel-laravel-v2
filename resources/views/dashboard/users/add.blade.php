@extends('layouts.dashboard')

@section('content')

    <div class="container">
        <div class="row my-5">
            <div class="col-md-12">

                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif

                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="basic-layout-colored-form-control">Agregar Usuario</h4>
                    </div>
                    <div class="card-body">

                        <form method="POST" action="{{ route('createUser') }}">
                            @csrf


                            <div class="form-body">
                                <div class="form-group">
                                    <label for="userinput6">Nombre</label>
                                    <input class="form-control" type="text" name="name">
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>


                                <div class="form-group">
                                    <label for="userinput6">Email</label>
                                    <input class="form-control" type="email" name="email">
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="userinput6">Rango</label>
                                    <select class="form-control border-primary @error('rank') is-invalid @enderror" name="rank" required>
                                        <option value="">Seleccionar rango</option>
                                        <option value="0">Administrador</option>
                                        <option value="1">Usuario</option>

                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="userinput6">Password</label>
                                    <input class="form-control border-primary @error('password') is-invalid @enderror" type="password" name="password">
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="userinput6">Confirmar Password</label>
                                    <input class="form-control border-primary" type="password" name="password_confirmation">
                                </div>

                                <div class="form-actions left">
                                    <button type="submit" class="btn btn-primary">Agregar Usuario</button>
                                    <a href="{{route('users') }}" class="btn btn-danger">
                                        Regresar</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>



@stop
