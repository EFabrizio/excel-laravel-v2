@extends('layouts.dashboard')

@section('content')

    <div class="container">
        <div class="row my-5">
            <div class="col-md-12">

                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="basic-layout-colored-form-control">Eliminar Usuario</h4>
                    </div>
                    <div class="card-body">

                        <form method="POST" action="{{ route('removeUser') }}">
                            @csrf

                            <input type="hidden" name="user_id" value="{{$user->id}}">

                            <div class="form-body">
                                <div class="form-group">
                                    <label for="userinput6">Nombre</label>
                                    <input class="form-control" type="text" name="name" value="{{ $user->name }}">
                                </div>


                                <div class="form-group">
                                    <label for="userinput6">Email</label>
                                    <input class="form-control" type="email" value="{{ $user->email }}" readonly>
                                </div>

                                <div class="form-group">
                                    <label for="userinput6">Rango</label>
                                    <input class="form-control border-primary" value="{{ $user->getRank() }}" readonly>

                                </div>

                                <div class="form-actions left">
                                    <button type="submit" class="btn btn-primary">Eliminar Usuario</button>
                                    <a href="{{route('users') }}" class="btn btn-danger">
                                        Regresar</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>



@stop
