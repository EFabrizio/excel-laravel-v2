@extends('layouts.dashboard')

@section('content')

    <div class="container">
        <div class="row my-5">
            <div class="col-md-12">

                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif

                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="basic-layout-colored-form-control">Editar Usuario</h4>
                    </div>
                    <div class="card-body">

                        <form method="POST" action="{{ route('updateUser') }}">
                            @csrf

                            <input type="hidden" name="user_id" value="{{$user->id}}">

                            <div class="form-body">
                                <div class="form-group">
                                    <label for="userinput6">Nombre</label>
                                    <input class="form-control" type="text" name="name" value="{{ $user->name }}">
                                </div>


                                <div class="form-group">
                                    <label for="userinput6">Email</label>
                                    <input class="form-control" type="email" value="{{ $user->email }}" readonly>
                                </div>

                                <div class="form-group">
                                    <label for="userinput6">Rango</label>
                                    <select class="form-control border-primary @error('rank') is-invalid @enderror" name="rank" required>
                                        <option value="">Seleccionar rango</option>
                                        <option {{ $user->rank == '0' ? 'selected' : null }} value="0">Administrador</option>
                                        <option {{ $user->rank == '1' ? 'selected' : null }} value="1">Estudiante</option>
                                        <option {{ $user->rank == '2' ? 'selected' : null }} value="2">Profesor</option>
                                        <option {{ $user->rank == '3' ? 'selected' : null }} value="3">Director de Carrera</option>

                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="userinput6">Password</label>
                                    <input class="form-control border-primary @error('password') is-invalid @enderror" type="password" name="password">
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="userinput6">Confirmar Password</label>
                                    <input class="form-control border-primary" type="password" name="password_confirmation">
                                </div>

                                <div class="form-actions left">
                                    <button type="submit" class="btn btn-primary">Editar Usuario</button>
                                    <a href="{{route('users') }}" class="btn btn-danger">
                                        Regresar</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>



@stop
