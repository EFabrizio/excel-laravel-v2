@extends('layouts.dashboard')

@section('content')

    <div class="container">
        <div class="row">

            @if(session()->has('error'))
                <div class="col-md-12 alert alert-danger mt-3">
                    <b>Error:</b> {{ session()->get('error') }}
                </div>
            @endif

                @if(session()->has('message'))
                    <div class="col-md-12 alert alert-success mt-3">
                        <b>Error:</b> {{ session()->get('message') }}
                    </div>
                @endif

            <div class="table-responsive table--no-card m-b-30">
                <table id="example" class="table table-borderless table-striped table-earning">
                    <thead>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Email</th>
                    <th>Rango</th>
                    <th>Opciones</th>
                    </thead>
                    <tbody>

                    @foreach($users as $user)

                        <tr>


                            <td>{{ $user->id }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>
                                @if ($user->rank === 0)
                                    <h5><span class="badge badge-primary">Administrador</span></h5>
                                @elseif ($user->rank == 1)
                                    <h5><span class="badge badge-secondary">Usuario</span></h5>
                                @endif
                            </td>
                            <td><a href="{{ route('userEdit', ['id' => $user->id]) }}"
                                   class="btn btn-primary">Editar</a>
                                <a href="{{ route('userDelete', ['id' => $user->id]) }}"
                                   class="btn btn-danger">Eliminar</a>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>

                </table>
            </div>

            <a href="{{ route('addUser') }}" class="btn btn-success">Agregar Usuario</a>

        </div>
    </div>



@stop
