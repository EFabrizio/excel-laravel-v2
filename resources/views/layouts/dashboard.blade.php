<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Sitio Web de Calvo">
    <meta name="author" content="Calvo">
    <meta name="keywords" content="Calvo">

    <!-- Title Page-->
    <title>Dashboard</title>

    <!-- Fontfaces CSS-->
    <link href="{{ asset('css/font-face.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('vendor/font-awesome-4.7/css/font-awesome.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('vendor/font-awesome-5/css/fontawesome-all.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('vendor/mdi-font/css/material-design-iconic-font.min.css') }}" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    {{--<!-- Vendor CSS-->--}}
    <link href="{{ asset('vendor/animsition/animsition.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css') }}" rel="stylesheet"
          media="all">
    <link href="{{ asset('vendor/wow/animate.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('vendor/css-hamburgers/hamburgers.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('vendor/slick/slick.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('vendor/select2/select2.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('vendor/perfect-scrollbar/perfect-scrollbar.css') }}" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="{{ asset('css/theme.css') }}" rel="stylesheet" media="all">
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>


</head>

<body>
<div class="page-wrapper">
    <!-- HEADER MOBILE-->
    <header class="header-mobile d-block d-lg-none">
        <div class="header-mobile__bar">
            <div class="container-fluid">
                <div class="header-mobile-inner">
                    <a class="logo" href="{{ route('home') }}">
                        <img src="{{ asset('img/calvo.png') }}" alt="Calvo"/>
                    </a>
                    <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                    </button>
                </div>
            </div>
        </div>
        <nav class="navbar-mobile">
            <div class="container-fluid">
                <ul class="navbar-mobile__list list-unstyled">
                    <li>
                        <a href="{{ route('home') }}">
                            <i class="fa fa-home"></i>Home</a>
                    </li>
                    <li class="has-sub">
                        <a class="js-arrow" href="#">
                            <i class="fas fa-calculator"></i>Herramientas</a>
                        <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                            <li>
                                <a href="{{ route('facias') }}">
                                    Facias</a>
                            </li>
                            <li>
                                <a href="{{ route('luminosos') }}">
                                    Luminosos</a>
                            </li>
                            <li>
                                <a href="{{ route('toldas') }}">
                                    Toldas</a>
                            </li>
                            <li>
                                <a href="{{ route('carpas') }}">
                                    Carpas</a>
                            </li>
                            <li>
                                <a href="{{ route('impresion') }}">
                                    Impresion G.F</a>
                            </li>
                            <li>
                                <a href="{{ route('papeleria') }}">
                                    Papeleria</a>
                            </li>
                            <li>
                                <a href="{{ route('corte') }}">
                                    Corte</a>
                            </li>
                        </ul>
                    </li>
                    @if(Auth::user()->rank == 0)
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fa fa-wrench"></i>Configuración</a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                    <a href="{{ route('varSettings') }}">
                                        VAR</a>
                                </li>
                                <li>
                                    <a href="{{ route('users') }}">
                                        Usuarios</a>
                                </li>
                            </ul>
                        </li>
                    @endif

                </ul>
            </div>
        </nav>
    </header>
    <!-- END HEADER MOBILE-->

    <!-- MENU SIDEBAR-->
    <aside class="menu-sidebar d-none d-lg-block">
        <div class="logo">
            <a href="#">
                <img src="{{ asset('img/calvo.png') }}" alt="Calvo">
            </a>
        </div>
        <div class="menu-sidebar__content js-scrollbar1">
            <nav class="navbar-sidebar">
                <ul class="list-unstyled navbar__list">
                    <li>
                        <a href="{{ route('home') }}">
                            <i class="fa fa-home"></i>Home</a>
                    </li>
                    <li class="has-sub">
                        <a class="js-arrow" href="#">
                            <i class="fas fa-calculator"></i>Herramientas</a>
                        <ul class="list-unstyled navbar__sub-list js-sub-list">
                            <li>
                                <a href="{{ route('facias') }}">
                                    Facias</a>
                            </li>
                            <li>
                                <a href="{{ route('luminosos') }}">
                                    Luminosos</a>
                            </li>

                            <li>
                                <a href="{{ route('toldas') }}">
                                    Toldas</a>
                            </li>

                            {{--<li class="has-sub">--}}
                                {{--<a class="js-arrow" href="#">--}}
                                    {{--Toldas </a>--}}
                                {{--<ul class="list-unstyled navbar__sub-list js-sub-list">--}}
                                    {{--<li>--}}
                                        {{--<a href="{{ route('toldas') }}">--}}
                                            {{--Toldas</a>--}}
                                    {{--</li>--}}

                                    {{--<li>--}}
                                        {{--<a href="{{ route('toldasStandard') }}">--}}
                                            {{--Toldas Alquileres</a>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            {{--</li>--}}
                            <li>
                                <a href="{{ route('carpas') }}">
                                    Carpas</a>
                            </li>

                            <li>
                                <a href="{{ route('impresion') }}">
                                    Impresión  G.F</a>
                            </li>

                            {{--<li class="has-sub">--}}
                                {{--<a class="js-arrow" href="#">--}}
                                    {{--Impresión</a>--}}
                                {{--<ul class="list-unstyled navbar__sub-list js-sub-list">--}}
                                    {{--<li>--}}
                                        {{--<a href="{{ route('impresion') }}">--}}
                                            {{--Impresión  G.F</a>--}}
                                    {{--</li>--}}

                                    {{--<li>--}}
                                        {{--<a href="{{ route('impresionStandard') }}">--}}
                                            {{--Impresiones estandarizadas</a>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            {{--</li>--}}


                            <li>
                                <a href="{{ route('papeleria') }}">
                                    Papeleria</a>
                            </li>
                            <li>
                                <a href="{{ route('corte') }}">
                                    Corte</a>
                            </li>

                            {{--<li>--}}
                                {{--<a href="{{ route('placas') }}">--}}
                                    {{--Placas estandarisadas</a>--}}
                            {{--</li>--}}

                            {{--<li>--}}
                                {{--<a href="{{ route('suvenir') }}">--}}
                                    {{--Suvenir</a>--}}
                            {{--</li>--}}

                            {{--<li>--}}
                                {{--<a href="{{ route('otros') }}">--}}
                                    {{--Otros</a>--}}
                            {{--</li>--}}

                            {{--<li>--}}
                                {{--<a href="{{ route('pernos') }}">--}}
                                    {{--Pernos</a>--}}
                            {{--</li>--}}
                        </ul>
                    </li>
                    @if(Auth::user()->rank == 0)

                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fa fa-wrench"></i>Configuración</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="{{ route('varSettings') }}">
                                        VAR</a>
                                </li>
                                <li>
                                    <a href="{{ route('users') }}">
                                        Usuarios</a>
                                </li>
                            </ul>
                        </li>

                    @endif
                </ul>
            </nav>
        </div>
    </aside>
    <!-- END MENU SIDEBAR-->

    <!-- PAGE CONTAINER-->
    <div class="page-container">
        <!-- HEADER DESKTOP-->
        <header class="header-desktop">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="header-wrap">
                        <div class="form-header">
                            @if(Auth::user()->rank == 0)
                                <h5 class="text-uppercase">Administrador</h5>
                            @elseif(Auth::user()->rank == 1)
                                <h5 class="text-uppercase">Usuario</h5>
                            @endif
                        </div>
                        <div class="header-button">
                            <div class="noti-wrap">
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        <div class="image">
                                            <img src="https://www.chcoc.org/wp-content/uploads/2016/01/avatar.jpeg"
                                                 alt="John Doe"/>
                                        </div>
                                        <div class="content">
                                            <a class="js-acc-btn" href="#">{{Auth::user()->name}}</a>
                                        </div>
                                        <div class="account-dropdown js-dropdown">
                                            <div class="info clearfix">
                                                <div class="image">
                                                    <a href="#">
                                                        <img
                                                            src="https://www.chcoc.org/wp-content/uploads/2016/01/avatar.jpeg"
                                                            alt="John Doe"/>
                                                    </a>
                                                </div>
                                                <div class="content">
                                                    <h5 class="name">
                                                        <a href="#">{{Auth::user()->name}}</a>
                                                    </h5>
                                                    <span class="email">{{Auth::user()->email}}</span>
                                                </div>
                                            </div>
                                            <div class="account-dropdown__body">
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-account"></i>Perfil</a>
                                                </div>
                                            </div>
                                            <div class="account-dropdown__footer">
                                                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                                    <i class="zmdi zmdi-power"></i>Cerrar Sesión</a>
                                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                                      style="display: none;">
                                                    {{ csrf_field() }}
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </header>
        <!-- HEADER DESKTOP-->

        <!-- MAIN CONTENT-->
        <div class="main-content">
            <div class="section__content section__content--p30">
                @yield('content')
            </div>
        </div>
        <!-- END MAIN CONTENT-->
        <!-- END PAGE CONTAINER-->
    </div>

</div>

<script
    src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
    crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<!-- Vendor JS       -->
<script src="{{ asset('vendor/slick/slick.min.js') }}">
</script>
<script src="{{ asset('vendor/wow/wow.min.js') }}"></script>
<script src="{{ asset('vendor/animsition/animsition.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap-progressbar/bootstrap-progressbar.min.js') }}">
</script>
<script src="{{ asset('vendor/counter-up/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('vendor/counter-up/jquery.counterup.min.js') }}">
</script>
<script src="{{ asset('vendor/circle-progress/circle-progress.min.js') }}"></script>
<script src="{{ asset('vendor/perfect-scrollbar/perfect-scrollbar.js') }}"></script>
<script src="{{ asset('vendor/chartjs/Chart.bundle.min.js') }}"></script>
<script src="{{ asset('vendor/select2/select2.min.js') }}">
</script>

<!-- Main JS-->
<script src="{{ asset('js/main.js') }}"></script>


</body>

</html>
<!-- end document-->
