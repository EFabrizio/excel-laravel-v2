<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['guest']], function () {
    Route::get('/', function () {
        return view('index');
    })->name('indexPortal');
});

Route::group(['middleware' => ['auth']], function () {
    Route::get('/panel', 'HomeController@dashboard')->name('home');
    Route::get('/panel/toldas/toldas', 'ToolsController@showToldas')->name('toldas');
    Route::get('/panel/toldas/estandarizadas', 'ToolsController@showToldas')->name('toldasStandard');
    Route::get('/panel/carpas', 'ToolsController@showCarpas')->name('carpas');
    Route::get('/panel/luminosos', 'ToolsController@showLuminosos')->name('luminosos');
    Route::get('/panel/papeleria', 'ToolsController@showPapeleria')->name('papeleria');
    Route::get('/panel/facias', 'ToolsController@showFacias')->name('facias');
    Route::get('/panel/impresion/gf', 'ToolsController@showImpresion')->name('impresion');
    Route::get('/panel/impresion/estandarizada', 'ToolsController@showImpresionStandard')->name('impresionStandard');
    Route::get('/panel/corte', 'ToolsController@showCorte')->name('corte');
    Route::get('/panel/placas', 'ToolsController@showPlacas')->name('placas');
    Route::get('/panel/otros', 'ToolsController@showOtros')->name('otros');
    Route::get('/panel/suvenir', 'ToolsController@showSuvenir')->name('suvenir');
    Route::get('/panel/pernos', 'ToolsController@showPernos')->name('pernos');
    Route::get('/var/data/{id}', 'ApiController@varData')->name('varData');
});


Route::group(['middleware' => ['auth', 'ranks:0']], function () {
    Route::get('/panel/configuracion/web', 'SettingsController@showWebSettings')->name('webSettings');
    Route::get('/panel/configuracion/var', 'SettingsController@showVarSettings')->name('varSettings');
    Route::get('/panel/configuracion/var/{id}', 'SettingsController@showItemOptions')->name('itemOptions');
    Route::get('/panel/configuracion/var/opcion/editar/{id}', 'SettingsController@showEditOption')->name('showOptionEdit');
    Route::get('/panel/configuracion/var/opcion/eliminar/{id}', 'SettingsController@showRemoveOption')->name('showOptionRemove');
    Route::post('/panel/configuracion/var/opcion/editar', 'SettingsController@editOption')->name('optionEdit');
    Route::post('/panel/configuracion/var/opcion/eliminar', 'SettingsController@removeOption')->name('optionRemove');
    Route::get('/panel/usuarios/', 'UsersController@usersPage')->name('users');
    Route::get('/panel/usuarios/agregar', 'UsersController@addUserPage')->name('addUser');
    Route::post('/panel/usuarios/agregar', 'UsersController@createUser')->name('createUser');
    Route::get('/panel/usuarios/editar/{id}', 'UsersController@usersEdit')->name('userEdit');
    Route::post('/panel/usuarios/editar', 'UsersController@updateUser')->name('updateUser');
    Route::get('/panel/usuarios/eliminar/{id}', 'UsersController@usersDelete')->name('userDelete');
    Route::post('/panel/usuarios/eliminar', 'UsersController@removeUser')->name('removeUser');
});


Auth::routes();
